module EitherLib where

-- 1.

lefts' :: [Either a b] -> [a]
lefts' = foldr f []
  where
    f (Left v) acc = v:acc
    f _        acc = acc

-- 2.

rights' :: [Either a b] -> [b]
rights' = foldr f []
  where
    f (Right v) acc = v:acc
    f _         acc = acc

-- 3.

partitionEithers' :: [Either a b]
                  -> ([a], [b])
partitionEithers' = foldr f ([], [])
  where
    f (Right v) (ls, rs) = (ls, v:rs)
    f (Left  v) (ls, rs) = (v:ls, rs)

-- 4.

eitherMaybe' :: (b -> c)
             -> Either a b
             -> Maybe c
eitherMaybe' f (Right b) = Just (f b)
eitherMaybe' _ _         = Nothing

-- 5.

either' :: (a -> c)
        -> (b -> c)
        -> Either a b
        -> c
either' f _ (Left v)  = f v
either' _ f (Right v) = f v


-- 6.

eitherMaybe'' :: (b -> c)
              -> Either a b
              -> Maybe c
eitherMaybe'' f = either' (const Nothing) (Just . f)
