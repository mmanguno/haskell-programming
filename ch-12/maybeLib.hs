module MaybeLib where

-- 1.

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust _       = True

isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing _       = False

-- 2.

mayybee :: b -> (a -> b) -> Maybe a -> b
mayybee b _ Nothing  = b
mayybee _ f (Just v) = f v

-- 3.

fromMaybe :: a -> Maybe a -> a
fromMaybe a Nothing  = a
fromMaybe _ (Just v) = v

-- 4.

listToMaybe :: [a] -> Maybe a
listToMaybe []    = Nothing
listToMaybe (x:_) = Just x

maybeToList :: Maybe a -> [a]
maybeToList Nothing  = []
maybeToList (Just v) = [v]

-- 5.

catMaybes :: [Maybe a] -> [a]
catMaybes = foldr f []
  where
    f (Just v) acc = v:acc
    f Nothing  acc = acc

-- 6.

flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe xs
  | length flipped < length xs = Nothing
  | otherwise                  = Just flipped
  where
    flipped = catMaybes xs
