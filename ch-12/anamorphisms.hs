module Anamorphisms where

-- 1.

myIterate :: (a -> a) -> a -> [a]
myIterate f a = a:(myIterate f (f a))

-- 2.

myUnfoldr :: (b -> Maybe (a, b))
          -> b
          -> [a]
myUnfoldr maybeFunc value =
  case (maybeFunc value) of
    Just (a, b) -> a:myUnfoldr maybeFunc b
    Nothing     -> []

-- 3.

betterIterate :: (a -> a) -> a -> [a]
betterIterate f x = myUnfoldr (\b -> Just (b, f b)) x
