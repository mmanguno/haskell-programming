module BinaryTrees where

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

-- 1.
unfold :: (a -> Maybe (a, b, a))
       -> a
       -> BinaryTree b
unfold f a =
  case f a of
    Just (a, b, a') -> Node (unfold f a) b (unfold f a')
    Nothing         -> Leaf

-- 2.
treeBuild :: Integer -> BinaryTree Integer
treeBuild n = unfold (\a -> if a < n then Just(a+1, a, a+1) else Nothing) 0
