module ChapterExercises where

import Data.List

-- Determine the kinds

-- 1.
-- a :: *

-- 2.
-- a :: *
-- f :: * -> *


-- String processing

-- 1.
notThe :: String -> Maybe String
notThe s
  | s == "the" = Nothing
  | otherwise  = Just s

replaceThe :: String -> String
replaceThe = concat . (intersperse " ") . replace . words
  where
    replace :: [String] -> [String]
    replace []     = []
    replace (x:xs) =
      case notThe x of
        Nothing -> "a":replace xs
        Just w  ->   w:replace xs


-- 2.
vowelWord :: String -> Bool
vowelWord = (flip elem) ['a', 'e', 'i', 'o', 'u'] . head

countTheBeforeVowel :: String -> Integer
countTheBeforeVowel s = go (words s) 0
  where
    go []       count = count
    go (x:[])   count = count
    go (x:y:xs) count =
      case notThe x of
        Nothing -> if vowelWord y then go (xs) count + 1 else go (y:xs) count
        Just w  -> go (y:xs) count


-- 3.

isVowel :: Char -> Bool
isVowel = (flip elem) ['a', 'e', 'i', 'o', 'u']

countVowels :: String -> Integer
countVowels = toInteger . length . (filter isVowel)


-- Validate the vowels

newtype Word' =
  Word' String
  deriving (Eq, Show)

vowels = "aeiou"

mkWord :: String -> Maybe Word'
mkWord w
  | consonants > vowels' = Nothing
  | otherwise            = Just (Word' w)
  where
    (consonants, vowels') = foldr f (0, 0) w
    f l (c, v) = if l `elem` vowels then (c, v + 1) else (c + 1, v)

-- It's only Natural

data Nat =
    Zero
  | Succ Nat
  deriving (Eq, Show)

natToInteger :: Nat -> Integer
natToInteger Zero     = 0
natToInteger (Succ n) = 1 + (natToInteger n)

integerToNat :: Integer -> Maybe Nat
integerToNat i
  | i <  0    = Nothing
  | otherwise = Just (go i)
  where
    go 0 = Zero
    go j = Succ (go (j - 1))


-- Small library for Maybe
-- see maybeLib.hs

-- Small library for Either
-- see eitherLib.hs

-- Write your own iterate and unfoldr
-- see anamorphisms.hs

-- Finally something other than a list!
-- see binaryTrees.hs
