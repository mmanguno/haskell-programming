module Bind where

import Control.Monad ( join )

-- join :: Monad m => m (m a) -> m a

bind :: Monad m => (a -> m b) -> m a -> m b
bind f mOfA = join $ fmap f mOfA
