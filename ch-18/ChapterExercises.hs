module ChapterExercises where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

-- Write Monad instances for the following.

-- 1.
data Nope a =
  NopeDotJpg
  deriving (Eq, Show)

instance Functor Nope where
  fmap _ _ = NopeDotJpg

instance Applicative Nope where
  pure _ = NopeDotJpg
  _ <*> _ = NopeDotJpg

instance Monad Nope where
  return = pure
  _ >>= _ = NopeDotJpg

instance Arbitrary (Nope a) where
  arbitrary = return NopeDotJpg

instance Eq a => EqProp (Nope a) where
  (=-=) = eq

nopeTest :: Nope (Int, String, Double)
nopeTest = undefined

-- 2.
data PhhhbbtttEither b a =
    Left' a
  | Right' b
  deriving (Eq, Show)


instance Functor (PhhhbbtttEither b) where
  fmap f (Left' a) = Left' (f a)
  fmap _ (Right' b) = Right' b


instance Applicative (PhhhbbtttEither b) where
  pure = Left'
  _        <*> Right' b = Right' b
  Right' f <*> _        = Right' f
  Left' f  <*> Left' a  = Left' (f a)


instance Monad (PhhhbbtttEither b) where
  return = pure
  Right' b >>= _ = Right' b
  Left' a  >>= f = f a


instance (Arbitrary a, Arbitrary b) => Arbitrary (PhhhbbtttEither b a) where
  arbitrary = frequency [ (1, Left' <$> arbitrary)
                        , (1, Right' <$> arbitrary) ]

instance (Eq a, Eq b) => EqProp (PhhhbbtttEither b a) where
  (=-=) = eq


phhhbbtttEitherTest :: PhhhbbtttEither (Int, String, Double)
                                       (Int, String, Double)
phhhbbtttEitherTest = undefined


-- 3.
newtype Identity a = Identity a
  deriving (Eq, Ord, Show)

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Applicative Identity where
  pure = Identity
  Identity f <*> Identity a = Identity (f a)

instance Monad Identity where
  return = pure
  Identity a >>= f = f a

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = Identity <$> arbitrary

instance Eq a => EqProp (Identity a) where
  (=-=) = eq


identityTest :: Identity (Int, String, Double)
identityTest = undefined


-- 4.
data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)


append :: (List a) -> (List a) -> (List a)
append Nil a = a
append (Cons x xs) ys = Cons x $ xs `append` ys

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons a b) = Cons (f a) (fmap f b)


instance Applicative List where
  pure x = (Cons x Nil)
  Nil <*> _   = Nil
  _   <*> Nil = Nil
  (Cons f fs) <*> a = (fmap f a) `append` (fs <*> a)


instance Monad List where
  return = pure
  Nil        >>= _ = Nil
  (Cons a b) >>= f = (f a) `append` (b >>= f)


instance Arbitrary a => Arbitrary (List a) where
  arbitrary = do
    a <- arbitrary
    l <- arbitrary
    frequency [ (1, return (Cons a l))
              , (2, return (Cons a Nil)) ]


instance Eq a => EqProp (List a) where
  (=-=) = eq


listTest :: List (Int, String, Double)
listTest = undefined

type TestName = String

-- comes from quickBatch's monad, applicative, and functor classes
test :: (Monad m, Applicative m, Functor m,
         Arbitrary a, Arbitrary b, Arbitrary c,
         Arbitrary (m a), Arbitrary (m b), Arbitrary (m c),
         Arbitrary (m (b -> c)), Arbitrary (m (a -> b)),
         CoArbitrary a, CoArbitrary b, Show a, Show (m a),
         Show a, Show (m a), Show (m (b -> c)), Show (m (a -> b)),
         EqProp (m a), EqProp (m b), EqProp (m c))
     => TestName -> m (a, b, c) -> IO ()
test name subject = do
  putStrLn name
  quickBatch $ functor subject
  quickBatch $ applicative subject
  quickBatch $ monad subject






-- Write the following functions using methods from Monad and Functor

-- 1.
j :: Monad m => m (m a) -> m a
j m = m >>= id

-- 2.
l1 :: Monad m => (a -> b) -> m a -> m b
l1 f m = f <$> m

-- 3.
l2 :: Monad m
   => (a -> b -> c) -> m a -> m b -> m c
l2 f mA mB = mBtoC >>= (\x -> mB >>= return . x)
   where mBtoC = mA >>= (return . f)

-- 4.
a :: Monad m => m a -> m (a -> b) -> m b
a mA mAtoB = mAtoB >>= (\x -> mA >>= return . x)


-- 5.
-- Makes an acc which is m [b], then traverses the list, appending
-- the successive bs to the acc with binds (prettified with do).
meh :: Monad m
    => [a] -> (a -> m b) -> m [b]
meh l f = go l f (return [])
  where go [] f acc = acc
        go (y:ys) f acc = do
          b <- (f y)
          bs <- (go ys f acc)
          return (b:bs)

-- 6.
flipType :: (Monad m) => [m a] -> m [a]
flipType l = meh l id


main :: IO ()
main = do
  test "Nope a:" nopeTest
  test "PhhhbbtttEither b a:" phhhbbtttEitherTest
  test "Identity a:" identityTest
  test "List a:" listTest
