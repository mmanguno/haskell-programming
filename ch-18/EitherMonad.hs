module EitherMonad where


data Sum a b =
    First a
  | Second b
  deriving (Eq, Show)


instance Functor (Sum a) where
  fmap f (First a)  = First a
  fmap f (Second b) = Second (f b)


instance Applicative (Sum a) where
  pure = Second
  _        <*> First a  = First a
  First f  <*> _        = First f
  Second f <*> Second b = Second (f b)


instance Monad (Sum a) where
  return = pure
  First a  >>= _ = First a
  Second b >>= f = f b
