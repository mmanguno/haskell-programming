module Division where


type Numerator = Integer
type Denominator = Integer
type Quotient = Integer


dividedBy' :: Numerator -> Denominator -> Quotient
dividedBy' = div

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
  where go n d count
         | n < d     = (count, n)
         | otherwise = go (n - d) d (count + 1)


data DividedResult =
    Result (Integer, Integer)
  | DividedByZero
  deriving (Eq, Show)


-- Doing this because I don't know about constraining
-- datatypes with Typeclasses, if that exists.
toResult :: Integral a => (a, a) -> DividedResult
toResult (a, b) = Result (toInteger a, toInteger b)


-- Pretty sloppy brute force pattern match, but it's
-- what the book's implying.
dividedByFix :: Integral a => a -> a -> DividedResult
dividedByFix num denom
 | denom == 0                = DividedByZero
 | (num >= 0) && (denom > 0) = toResult (a, rem)
 | (num >= 0) && (denom < 0) = toResult (negate a, rem)
 | (num < 0)  && (denom < 0) = toResult (a, rem)
 | (num < 0)  && (denom > 0) = toResult (negate a, rem)
 where
    (a, rem) = go (abs num) (abs denom) 0
    go n d count
      | n < d     = (count, n)
      | otherwise = go (n - d) d (count + 1)
