module Cipher where

import Data.Char


caesar :: Int -> String -> String
caesar shift []     = []
caesar shift (x:xs) = letter : (caesar shift xs)
  where
    shifted = shift + (ord x)
    index   = mod (shifted - 97) 26
    letter  = chr (index + 97)


unCaesar :: Int -> String -> String
unCaesar = caesar . negate
