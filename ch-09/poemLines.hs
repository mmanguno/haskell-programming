module PoemLines where

mySplitter :: Char -> String -> [String]
mySplitter _ [] = []
mySplitter c s  = first : (myLines rest)
  where
    first = takeWhile (/= c) s
    rest  = drop 1 . dropWhile (/= c) $ s

mySplitter' :: Char -> String -> [String]
mySplitter' _ [] = []
mySplitter' c (x:xs)
  | c == x    = mySplitter' c xs
  | otherwise = (x:takeWhile (/=c) xs) : mySplitter' c (dropWhile (/=c) xs)

myLines' :: String -> [String]
myLines' = mySplitter '\n'

myWords' :: String -> [String]
myWords' = mySplitter ' '

firstSen = "Tyger Tyger, burning bright\n"
secondSen = "In the forests of the night\n"
thirdSen = "What immortal hand or eye\n"
fourthSen = "Could frame thy fearful\
            \ symmetry?"
sentences = firstSen ++ secondSen
          ++ thirdSen ++ fourthSen

-- putStrLn sentences -- should print
-- Tyger Tyger, burning bright
-- In the forests of the night
-- What immortal hand or eye
-- Could frame thy fearful symmetry?


-- Implement this
myLines :: String -> [String]
myLines [] = []
myLines s  = first : (myLines rest)
  where
    first = takeWhile (/= '\n') s
    rest  = drop 1 . dropWhile (/= '\n') $ s

myLines'' :: String -> [String]
myLines'' []       = []
myLines'' ('\n':xs) = myLines'' xs
myLines'' xs       = takeWhile (/='\n') xs : myLines'' (dropWhile (/='\n') xs)


-- What we want 'myLines sentences'
-- to equal
shouldEqual =
  [ "Tyger Tyger, burning bright"
  , "In the forests of the night"
  , "What immortal hand or eye"
  , "Could frame thy fearful symmetry?"
  ]


-- The main function here is a small test
-- to ensure you've written your function
-- correctly.
main :: IO ()
main =
  print $
  "Are they equal? "
  ++ show (myLines sentences
           == shouldEqual)
