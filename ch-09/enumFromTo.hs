module EnumFromTo where

-- gets to the root of it
-- need to constrain to
eft :: (Ord a, Enum a) => a -> a -> [a]
eft a b
  | a > b     = []
  | a == b    = [a]
  | otherwise = a:(eft (succ a) b)

-- Now we're just renaming and applying types
eftBool :: Bool -> Bool -> [Bool]
eftBool = eft

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd = eft

eftInt :: Int -> Int -> [Int]
eftInt = eft

eftChar :: Char -> Char -> [Char]
eftChar = eft
