module Filtering where

-- 1.

multiplesOfThree = filter (\x -> (rem x 3) == 0) [1..30]

-- 2.

howManyMultiplesOfThree = length . filter (\x -> (rem x 3) == 0) $ [1..30]

-- 3.

myFilter :: String -> [String]
myFilter = filter (\x -> not (elem x ["the", "a", "an"])) . words
