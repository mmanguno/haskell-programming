module MyZip where

zip' :: [a] -> [b] -> [(a, b)]
zip' [] []         = []
zip' xs []         = []
zip' [] ys         = []
zip' (x:xs) (y:ys) = (x, y) : zip' xs ys


zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f [] []         = []
zipWith' f xs []         = []
zipWith' f [] ys         = []
zipWith' f (x:xs) (y:ys) = (f x y) : zipWith' f xs ys

zip'' :: [a] -> [b] -> [(a, b)]
zip'' = zipWith' (\x -> \y -> (x, y))
