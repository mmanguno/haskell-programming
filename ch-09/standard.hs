module Standard where

import Data.Maybe

-- 1.

myOr :: [Bool] -> Bool
myOr []     = False
myOr (x:xs) = if x == True then True else myOr xs


-- 2.

myAny :: (a -> Bool) -> [a] -> Bool
myAny f [] = False
myAny f (x:xs) = if f x then True else myAny f xs


-- 3.

myElem :: Eq a => a -> [a] -> Bool
myElem a []     = False
myElem a (x:xs) = if a == x then True else myElem a xs

myElem' :: Eq a => a -> [a] -> Bool
myElem' a = myAny (==a)


-- 4.

myReverse :: [a] -> [a]
myReverse []     = []
myReverse (x:xs) = (myReverse xs) ++ [x]


-- 5.

squish :: [[a]] -> [a]
squish []     = []
squish (x:xs) = x ++ squish xs


-- 6.

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f [] = []
squishMap f (x:xs) = (f x) ++ (squishMap f xs)

-- 7.

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

-- 8.

-- I don't like it bottoming out on empty list, so using Maybe
myMaximumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
myMaximumBy f []       = Nothing
myMaximumBy f (x:[])   = Just x
myMaximumBy f (x:y:xs) = myMaximumBy f (greater:xs)
  where greater
          | f x y == GT = x
          | otherwise   = y

-- 9.

-- I don't like it bottoming out on empty list, so using Maybe
myMinimumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
myMinimumBy f []       = Nothing
myMinimumBy f (x:[])   = Just x
myMinimumBy f (x:y:xs) = myMinimumBy f (lesser:xs)
  where lesser
          | f x y == LT = x
          | otherwise   = y


-- 10.

myMaximum :: (Ord a) => [a] -> Maybe a
myMaximum = myMaximumBy compare


myMinimum :: (Ord a) => [a] -> Maybe a
myMinimum = myMinimumBy compare
