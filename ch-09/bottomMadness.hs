module BottomMadness where

-- _|_
one = [x^y | x <- [1..5], y <- [2, undefined]]

-- returns a value; [1]
two = take 1 $ [x^y | x <- [1..5], y <- [2, undefined]]

-- _|_
three = sum [1, undefined, 3]

-- returns a value; 3
four = length [1, 2, undefined]

-- _|_
five = length $ [1, 2, 3] ++ undefined

-- returns a value; 2
six = take 1 $ filter even [1, 2, 3, undefined]

-- _|_
seven = take 1 $ filter even [1, 3, undefined]

-- returns a value; 1
eight = take 1 $ filter odd [1, 3, undefined]

-- returns a value; [1, 3]
nine = take 2 $ filter odd [1, 3, undefined]

-- _|_
ten = take 3 $ filter odd [1, 3, undefined]
