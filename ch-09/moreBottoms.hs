module MoreBottom where

-- 1.
-- _|_


-- 2.
-- yes; [2]


-- 3.
-- _|_

-- 4.
-- The function returns a list of Bools. The value will be True at the
-- index if the letter is a vowel, and False otherwise.
--
-- Prelude> itIsMystery "Hello World!"
-- [False,True,False,False,True,False,False,True,False,False,False,False]

-- 5.
-- a) list of 1 through 10's squares ([1,4,9,16,25,36,49,64,81,100])
-- b) a list of the minimums of each list ([1, 10, 20])
-- c) a list of the sums of each of the lists from 1 to 5
--    (1 + 2 + 3 + 4 + 5) = 15. ([15, 15, 15])

-- 6.

import Data.Bool

result = map (\x -> bool x (-x) (x == 3)) [1..10]
