module ChapterExercises where

import Data.Char

-- Data.Char

-- 2.

onlyUppers :: String -> String
onlyUppers = filter isUpper

-- 3.

capitalize :: String -> String
capitalize [] = []
capitalize (x:xs) = toUpper x : xs

-- 4.

capAll :: String -> String
capAll [] = []
capAll (x:xs) = toUpper x : capAll xs

-- 5.

capHead :: String -> Char
capHead x = toUpper (head x)

-- 6.

capHead' :: String -> Char
capHead' x = toUpper . head $ x

capHead'' :: String -> Char
capHead'' = toUpper . head


-- Ciphers

-- (see cipher.hs)

-- Writing your own standard functions

-- (see standard.hs)
