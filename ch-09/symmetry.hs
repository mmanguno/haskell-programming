module Symmetry where

myWords :: String -> [String]
myWords [] = []
myWords s  = first : (myWords rest)
  where
    first = takeWhile (/= ' ') s
    rest  = drop 1 . dropWhile (/= ' ') $ s


myWords' :: String -> [String]
myWords' []       = []
myWords' (' ':xs) = myWords' xs
myWords' xs       = takeWhile (/=' ') xs : myWords' (dropWhile (/=' ') xs)
