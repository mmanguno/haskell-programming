import Control.Monad
import Data.Char (toLower)
import System.Exit (exitSuccess)


alphanumeric = ['a'..'z'] ++ ['0'..'9']
normalize :: String -> String
normalize = (filter (`elem` alphanumeric)) . (fmap toLower)

palindrome :: IO ()
palindrome = forever $ do
  line <- getLine
  let normalized = normalize line
  case (normalized == reverse normalized) of
    True  -> putStrLn "It's a palindrome"
    False -> exitSuccess
