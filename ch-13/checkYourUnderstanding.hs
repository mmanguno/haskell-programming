-- 1.
-- forever, when

-- 2.
-- Data.Bits, Database.Blacktip.Types

-- 3.
-- Type and data constructors

-- 4.
-- a.
-- MV  -> Constrol.Concurrent.Mvar
-- FPC -> Filesystem.Path.CurrentOS
-- CC  -> Control.Current

-- b.
-- Filesystem.writeFile

-- c.
-- Control.Monad
