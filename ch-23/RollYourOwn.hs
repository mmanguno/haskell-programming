module RollYourOwn where

import System.Random
import RandomExample

-- 1.

rollsToGetToN :: Int -> StdGen -> Int
rollsToGetToN n g = go n 0 0 g
  where
    go :: Int -> Int -> Int -> StdGen -> Int
    go n sum count gen
      | sum >= n = count
      | otherwise =
        let (die, nextGen) =
              randomR (1, 6) gen
        in go n (sum + die)
              (count + 1) nextGen

-- 2.

rollsCountLogged :: Int
                 -> StdGen
                 -> (Int, [Die])
rollsCountLogged n g = go n 0 (0, []) g
  where
    go :: Int -> Int -> (Int, [Die]) -> StdGen -> (Int, [Die])
    go n sum diceCount gen
      | sum >= n = diceCount
      | otherwise =
        let (die, nextGen) =
              randomR (1, 6) gen
            newCount = (fst diceCount) + 1
            dieList  = (intToDie die) : (snd diceCount)
        in go n (sum + die)
              (newCount, dieList) nextGen
