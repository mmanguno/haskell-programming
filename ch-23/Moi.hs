module Moi where

import Test.Hspec

newtype Moi s a =
  Moi { runMoi :: s -> (a, s) }


instance Functor (Moi s) where
  fmap f (Moi g) = Moi g'
    where g' s' = (f a, s)
            where (a, s) = g s'
  -- I would like to use the functor of a tuple
  -- here; if only the type was s -> (s, a)


instance Applicative (Moi s) where
  pure a = Moi (\s -> (a, s))

  (Moi f) <*> (Moi g) = Moi g'
    where g' s' = (aToB a, s)
            where (aToB, _) = f s'
                  (a, s)    = g s'


instance Monad (Moi s) where
  return = pure

  (Moi f) >>= g =
    Moi $ \s -> runMoi (g (fst (f s))) s



-- simple sanity checks since I'm unsure about quickBatch
-- and function equality
main :: IO ()
main = hspec $ do
  describe "functor" $ do
    it "fmaps correctly" $ do
      runMoi ((+1) <$> (Moi $ \s -> (0, s))) 0
        `shouldBe` (1, 0)

  describe "applicative" $ do
    it "lifts with pure" $ do
      runMoi (pure 0) 1
        `shouldBe` (0, 1)
    it "applies" $ do
      let mF = Moi (\s -> ((+1), s))
          mG = Moi (\t -> (0, t))
      runMoi (mF <*> mG) 5
        `shouldBe` (1, 5)

  describe "monad" $ do
    it "binds correctly" $ do
      let g a = Moi (\f -> (a, f))
          mF  = Moi (\t -> (0, t))
      runMoi (mF >>= g) 1
        `shouldBe` (0, 1)
