module Validation where

import Control.Applicative
import Data.Monoid

import Test.QuickCheck ( Arbitrary, arbitrary, frequency )
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data Validation e a =
    Failure e
  | Success a
  deriving (Eq, Show)


instance Functor (Validation e) where
  fmap _ (Failure e) = Failure e
  fmap f (Success a) = Success $ f a


instance Monoid e => Applicative (Validation e) where
  pure = Success
  Failure e <*> Success a  = Failure e
  Success a <*> Failure e  = Failure e
  Failure e <*> Failure e' = Failure $ e <> e'
  Success f <*> Success a  = Success $ f a


instance (Arbitrary a, Arbitrary e) => Arbitrary (Validation e a) where
  arbitrary =
    frequency [ (1, Failure <$> arbitrary)
              , (1, Success <$> arbitrary) ]

instance (Eq a, Eq e) => EqProp (Validation a e) where
  (=-=) = eq

tester :: Validation [Int] (Int, Int, Int)
tester = Failure [1]

main :: IO ()
main = do
  quickBatch $ applicative $ tester
