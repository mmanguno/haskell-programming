module Instances where

import Data.Monoid

import Test.QuickCheck ( Arbitrary, arbitrary, frequency )
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes


-- 1.

data Pair a = Pair a a deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair a a') = Pair (f a) (f a')

instance Applicative Pair where
  pure a = Pair a a
  Pair f f' <*> Pair a a' = Pair (f a) (f' a')

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = Pair <$> arbitrary
                   <*> arbitrary

instance Eq a => EqProp (Pair a) where
  (=-=) = eq

pairTester :: Pair (Int, Int, Int)
pairTester = Pair (1, 2, 3) (4, 5, 6)


-- 2.

data Two a b = Two a b deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two a b) = Two a $ f b

instance Monoid a => Applicative (Two a) where
  pure = Two mempty
  Two a f <*> Two a' b = Two (a <> a') $ f b

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = Two <$> arbitrary
                  <*> arbitrary

instance (Eq a, Eq b) => EqProp (Two a b) where
  (=-=) = eq

type SInt = Sum Int
twoTester :: Two (SInt, SInt, SInt) (SInt, SInt, SInt)
twoTester = Two (1, 2, 3) (4, 5, 6)


-- 3.

data Three a b c = Three a b c deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three a b c) = Three a b $ f c

instance (Monoid a, Monoid b) => Applicative (Three a b) where
  pure = Three mempty mempty
  (Three a b f) <*> (Three a' b' c) = Three (a <> a') (b <> b') (f c)

instance (Arbitrary a, Arbitrary b, Arbitrary c)
      => Arbitrary (Three a b c) where
  arbitrary = Three <$> arbitrary
                    <*> arbitrary
                    <*> arbitrary

instance (Eq a, Eq b, Eq c) => EqProp (Three a b c) where
  (=-=) = eq

threeTester :: Three (SInt, SInt, SInt)
                     (SInt, SInt, SInt)
                     (SInt, SInt, SInt)
threeTester = Three (1, 2, 3) (4, 5, 6) (7, 8, 9)


-- 4.
data Three' a b = Three' a b b deriving (Eq, Show)

instance Functor (Three' a) where
  fmap f (Three' a b b') = Three' a (f b) (f b')

instance Monoid a => Applicative (Three' a) where
  pure b = Three' mempty b b
  (Three' a f f') <*> (Three' a' b b') = Three' (a <> a') (f b) (f' b')

instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = Three' <$> arbitrary
                     <*> arbitrary
                     <*> arbitrary

instance (Eq a, Eq b) => EqProp (Three' a b) where
  (=-=) = eq

three'Tester :: Three' (SInt, SInt, SInt) (SInt, SInt, SInt)
three'Tester = Three' (1, 2, 3) (4, 5, 6) (7, 8, 9)


-- 5.
data Four a b c d = Four a b c d deriving (Eq, Show)

instance Functor (Four a b c) where
  fmap f (Four a b c d) = Four a b c $ f d

instance (Monoid a, Monoid b, Monoid c) => Applicative (Four a b c) where
  pure = Four mempty mempty mempty
  (Four a b c f) <*> (Four a' b' c' d) =
    Four (a <> a') (b <> b') (c <> c') (f d)

instance (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d)
      => Arbitrary (Four a b c d) where
  arbitrary = Four <$> arbitrary
                   <*> arbitrary
                   <*> arbitrary
                   <*> arbitrary

instance (Eq a, Eq b, Eq c, Eq d) => EqProp (Four a b c d) where
  (=-=) = eq

fourTester :: Four (SInt, SInt, SInt)
                   (SInt, SInt, SInt)
                   (SInt, SInt, SInt)
                   (SInt, SInt, SInt)
fourTester = Four (1, 2, 3) (4, 5, 6) (7, 8, 9) (10, 11, 12)


-- 6.
data Four' a b = Four' a a a b deriving (Eq, Show)

instance Functor (Four' a) where
  fmap f (Four' a a' a'' b) = Four' a a' a'' $ f b

instance (Monoid a) => Applicative (Four' a) where
  pure = Four' mempty mempty mempty
  Four' a a' a'' f <*> Four' c c' c'' b =
    Four' (a <> c) (a' <> c') (a'' <> c'') (f b)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Four' a b) where
  arbitrary = Four' <$> arbitrary
                    <*> arbitrary
                    <*> arbitrary
                    <*> arbitrary

instance (Eq a, Eq b) => EqProp (Four' a b) where
  (=-=) = eq

four'Tester :: Four' (SInt, SInt, SInt) (SInt, SInt, SInt)
four'Tester = Four' (1, 2, 3) (4, 5, 6) (7, 8, 9) (10, 11, 12)


main :: IO ()
main = do
  putStrLn "\nPair:"
  quickBatch $ applicative $ pairTester
  putStrLn "\nTwo:"
  quickBatch $ applicative $ twoTester
  putStrLn "\nThree:"
  quickBatch $ applicative $ threeTester
  putStrLn "\nThree':"
  quickBatch $ applicative $ three'Tester
  putStrLn "\nFour:"
  quickBatch $ applicative $ fourTester
  putStrLn "\nFour':"
  quickBatch $ applicative $ four'Tester
