module ListApplicative
  ( List (Nil, Cons) )
where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)


append :: (List a) -> (List a) -> (List a)
append Nil a = a
append (Cons x xs) ys = Cons x $ xs `append` ys


instance Semigroup (List a) where
  a   <> Nil = a
  Nil <> a   = a
  l   <> l'  = append l l'


instance Monoid (List a) where
  mempty = Nil


instance Functor List where
  fmap _ Nil         = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)


instance Applicative List where
  pure a = (Cons a Nil)
  Nil         <*> a   = Nil
  a           <*> Nil = Nil
  (Cons f fs) <*> l   = (f <$> l) <> (fs <*> l)


instance Arbitrary a => Arbitrary (List a) where
  arbitrary = do
    a <- arbitrary
    l <- arbitrary
    frequency [ (1, return (Cons a l))
              , (2, return (Cons a Nil)) ]


instance Eq a => EqProp (List a) where
  (=-=) = eq


main :: IO ()
main = do
  quickBatch (monoid (Cons (1 :: Int) Nil))
  quickBatch (applicative ((Cons (1, 2, 3) Nil) :: List (Int, Int, Int)))
