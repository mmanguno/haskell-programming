module ChapterExercises where

import Control.Applicative ( liftA3 )

-- Specialize the types of the methods.

-- 1.
-- []

-- pure  :: a -> [a]
-- (<*>) :: [(a -> b)] -> [a] -> [b]

-- 2.
-- IO

-- pure :: a -> IO a
-- (<*>) :: IO (a -> b) -> IO a -> IO b

-- 3.
-- (,) a'

-- pure :: a -> (a', a)
-- (<*>) :: (a', (a -> b)) -> (a', a) -> (a', b)

-- 4.
-- (->) e

-- pure :: a -> (e -> a)
-- (<*>) :: (e -> (a -> b)) -> (e -> a) -> (e -> b)


-- Write instances

-- see Instances.h


-- Combinations

stops :: String
stops = "pbtdkg"

vowels :: String
vowels = "aeiou"

combos :: [a] -> [b] -> [c] -> [(a, b, c)]
combos = liftA3 (,,)


main :: IO ()
main = print $ combos stops vowels stops
