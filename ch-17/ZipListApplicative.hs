module ZipListApplicative where

import Control.Applicative
import Data.Monoid

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

import ListApplicative


-- Some utility functions for the List datatype

take' :: Int -> List a -> List a
take' _ Nil = Nil
take' 0 _   = Nil
take' 1 (Cons x xs) = Cons x Nil
take' i (Cons x xs) = Cons x $ take' (i-1) xs

repeat' :: a -> List a
repeat' a = Cons a (repeat' a)

zipWith' :: (a -> b -> c) -> List a -> List b -> List c
zipWith' _ Nil _ = Nil
zipWith' _ _ Nil = Nil
zipWith' f (Cons x xs) (Cons y ys) = Cons (x `f` y) (zipWith' f xs ys)



-- The ZipList' datatype

newtype ZipList' a =
  ZipList' (List a)
  deriving (Eq, Show)


instance Semigroup a => Semigroup (ZipList' a) where
   ZipList' xs  <> ZipList' ys  = ZipList' $ zipWith' (<>) xs ys


instance Monoid a => Monoid (ZipList' a) where
   mempty = pure mempty


instance Functor ZipList' where
  fmap f (ZipList' xs) = ZipList' $ fmap f xs


instance Applicative ZipList' where
  pure = ZipList' . repeat'
  ZipList' fs <*> ZipList' xs = ZipList' $ zipWith' ($) fs xs



-- Testing


instance Arbitrary a => Arbitrary (ZipList' a) where
  arbitrary = ZipList' <$> arbitrary


instance Eq a => EqProp (ZipList' a) where
  xs =-= ys = xs' `eq` ys'
    where xs' = let (ZipList' l) = xs
                in take' 3000 l
          ys' = let (ZipList' l) = ys
                in take' 3000 l


type IntZip = ZipList' (Int, Int, Int)

main :: IO ()
main = do
  quickBatch $ monoid $ (ZipList' (Cons (1 :: Sum Int) Nil))
  quickBatch $ applicative $ (ZipList' (Cons (1, 2, 3) Nil) :: IntZip)
