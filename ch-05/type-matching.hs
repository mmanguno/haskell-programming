-- a.
not :: Bool -> Bool

-- b.
length :: [a] -> Int

-- c.
concat :: [[a]] -> [a]

-- d.
head :: [a] -> a

-- e.
(<) :: Ord a => a -> a -> Bool
