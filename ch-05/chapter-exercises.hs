-- Multiple choice

-- 1.
-- c) a list whose elements are all of some type a

-- 2.
-- a) take a list of strings as an argument

-- 3.
-- b) returns one element of type a from a list

-- 4.
-- c) takes a tuple argument and returns the first value


-- Determine the type

-- 1.
-- a) 54. Num a => a.
-- b) (0, "doge"). Num a => (a, [Char])
-- c) (0, "doge"). (Integer, [Char])
-- d) False. Bool.
-- e) 5. Int.
-- f) False. Bool.

-- 2.
-- Num a => a.

-- 3.
-- Num a => a -> a

-- 4.
-- Fractional a => a

-- 5.
-- [Char]


-- Does it compile?

-- 1. Does not compile. bigNum is not a function. This cannot be fixed in its
--    current form (what to do with 10? then we could fix).

-- 2. Compiles.

-- 3. Does not compile. b is not a function. Possible fix:
-- a = (+)
-- b = 5 -- unused
-- c = a 10
-- d = c 200

-- 4. Does not compile. b is not defined yet, and c is not defined at all.
--    Possible fix:
-- b = 100000
-- a = 12 + b


-- Type variable or specific type constructor?

-- 2. f :: zed -> Zed -> Blah
--         [0]    [1]     [2]

-- [0]: fully polymorphic
-- [1] and [2]: concrete

-- 3. f :: Enum b => a -> b -> C
--                  [0]  [1]  [2]

-- [0]: fully polymorphic
-- [1]: constrained polymorphic
-- [2]: concrete

-- 4. f :: f -> g -> C
--        [0]  [1]  [2]

-- [0] and [1]: fully polymorphic
-- [2]: concrete


-- Write a type signature

-- 1. functionH :: [a] -> a

-- 2. functionC :: (Ord a) => a -> a -> Bool

-- 3. functionS :: (a, b) -> b


-- Given a type, write the function

-- 1.
-- i :: a -> a
-- i a = a

-- 2.
-- c :: a -> b -> a
-- c a b = a

-- 3.
-- c'' :: b -> a -> b
-- c'' b a = b

-- 4.
-- c' :: a -> b -> b
-- c' a b = b

-- 5.
-- r :: [a] -> [a]
-- r a = reverse a

-- 6.
-- co :: (b -> c) -> (a -> b) -> a -> c
-- co bToC aToB a = btoC $ aToB a

-- 7.
-- a :: (a -> c) -> a -> a
-- a aToC a = a

-- 8.
-- a' :: (a -> b) -> a -> b
-- a' aToB a = aToB a


-- Fix it

-- 1.
-- (see sing.hs)

-- 2.
-- (see sing.hs)

-- 3.
-- (see arith3broken.hs)


-- Type-Kwon-Do

-- 1.

f :: Int -> String
f = undefined

g :: String -> Char
g = undefined

h :: Int -> Char
h i = g (f i)

-- 2.

data A
data B
data C

q :: A -> B
q = undefined

w :: B -> C
w = undefined

e :: A -> C
e a = w (q a)


-- 3.

data X
data Y
data Z

xz :: X -> Z
xz = undefined

yz :: Y -> Z
yz = undefined

xform :: (X, Y) -> (Z, Z)
xform (x, y) = ((xz x), (yz y))


-- 4.
munge :: (x -> y)
      -> (y -> (w, z))
      -> x
      -> w
munge xToY yToWZ x = head (yToWZ (xToY x))
