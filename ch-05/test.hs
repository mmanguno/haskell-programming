f :: (Num a, Num b) => a -> b -> (a, b)
f a b = (a, b)

f' :: Num a => a -> a -> (a, a)
f' a b = (a, b)
