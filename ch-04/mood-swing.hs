-- 1)
-- The name is "Mood"

-- 2)
-- It can use "Blah" or "Woot"

--3)
-- It should be "changeMood :: Mood -> Mood". It can be either Mood, not
-- just "Woot". Also, you can't use a Constructor in a type signature.

-- 4)


data Mood = Blah | Woot deriving Show
changeMood :: Mood -> Mood
changeMood Blah = Woot
changeMood _ = Blah
