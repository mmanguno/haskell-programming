awesome = ["Papuchon", "curry", ":)"]
also = ["Quake", "The Simons"]
allAwesome = [awesome, also]

-- 1)
-- length :: [a] -> Int
-- One argument, of type "List". It would resolve to an Integer.

-- 2)
-- a. 5
-- b. 3
-- c. 2
-- d. 5

-- 3)
-- 6 / 3 works. The other does not, because the return type of length
-- is "Int". Int is not an instance of Fractional, and therefore cannot
-- be used.

-- 4)
-- div 6 (length [1, 2, 3])

-- 5)
-- Type of the expression is Bool. Expect "True".

-- 6) Type of the expression is Bool. Expect "False".

-- 7)
-- a. Works. Reduces to "True".
-- b. Does not work. The list must be of one type.
-- c. Works. Reduces to 5.
-- d. Works. Reduces to "False".
-- e. Does not work. (&&) takes two Bools, and 9 is a Num.

-- 8)
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome x = x == reverse x

-- 9)
myAbs :: Integer -> Integer
myAbs x = if x < 0
          then (-x)
          else x

-- 10)
f :: (a, b) -> (c, d) -> ((b, d), (a, c))
f (a, b) (c, d) = (seconds, firsts)
  where firsts = (fst (a, b), fst (c, d))
        seconds = (snd (a, b), snd (b, d))


-- Correcting syntax

-- 1)
x = (+)

g xs = x w 1
  where w = length xs

-- 2)
myId = \x -> x

-- 3)
h (a, b) = a


-- Match the function names to their types

-- 1)
-- c) Show a => a -> String

-- 2)
-- b) Eq => a -> a -> Bool

-- 3)
-- a) (a, b) -> a

-- 4)
-- d) (+) :: Num a => a -> a -> a
