-- 1)
-- true is lowercase
one = not True && True


-- 2)
-- must be double equals
two = not (x == 6)

-- 3)
-- No mistake.
three = (1 * 2) > 5

-- 4)
-- Need quotes around the characters
fourth = ["Merry"] > ["Happy"]

-- 5)
-- Cannot concatenate different datatypes.
-- Not really a meaningful operation, but we can do:
five = ['1', '2', '3'] ++ "look at me!"
