module GreetIfCool2 where

greetIfCool :: String -> IO ()
greetIfCool coolness =
  if cool coolness
     then putStrLn "cool enough"
  else
    putStrLn "hmmm"
  where cool v =
          v == "hmmmmmm"
