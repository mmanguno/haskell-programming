module Ciphers
  ( caesar
  , unCaesar
  , vigenere
  , unVigenere
  ) where

import Data.Char (chr, ord)
import Text.Read (readMaybe) -- to read String -> Int safely

-- cipher library functions

shift :: Int -> Char -> Char
shift i c = letter
  where
    start   = ord 'a'
    shifted = i + (ord c)
    index   = mod (shifted - start) 26
    letter  = chr (index + start)


unshift :: Int -> Char -> Char
unshift = shift . negate


-- Vigenere which can shift or unshift, using some operation 'op'
vigenere' :: (Int -> Char -> Char) -> String -> String -> String
vigenere' _ ""       word     = word
vigenere' op keyword []       = []
vigenere' op keyword (' ':xs) =     ' ' : (vigenere' op keyword xs)
vigenere' op keyword (x:xs)   = shifted : (vigenere' op newKey xs)
  where
    shifter = head keyword
    shiftBy = ord shifter - ord 'a'
    shifted = op shiftBy x
    newKey  = tail keyword ++ [shifter]


vigenere :: String -> String -> String
vigenere = vigenere' shift


unVigenere :: String -> String -> String
unVigenere = vigenere' unshift


caesar :: Int -> String -> String
caesar i []     = []
caesar i (x:xs) = (shift i x): (caesar i xs)


unCaesar :: Int -> String -> String
unCaesar = caesar . negate


-- cipher user interface

-- caesar cipher
promptCaesar :: IO ()
promptCaesar = do
  putStrLn "Encode (1), or decode (2)?"
  choice <- getLine
  case choice of
    "1" -> encodeCaesar
    "2" -> decodeCaesar

encodeCaesar :: IO ()
encodeCaesar = do
  putStrLn "What do you want to encode?"
  string <- getLine
  putStrLn "With what shift?"
  shiftStr <- getLine
  case (readMaybe shiftStr) :: Maybe Int of
    (Just v) -> putStrLn $ "Encoded: " ++ caesar v string
    Nothing  -> putStrLn $ "Please provide a valid Int."

decodeCaesar :: IO ()
decodeCaesar = do
  putStrLn "What do you want to decode?"
  string <- getLine
  putStrLn "With what shift?"
  shiftStr <- getLine
  case (readMaybe shiftStr) :: Maybe Int of
    (Just v) -> putStrLn $ "Decoded: " ++ unCaesar v string
    Nothing  -> putStrLn $ "Please provide a valid Int."

-- vigenere cipher
promptVigenere :: IO ()
promptVigenere = do
  putStrLn "Encode (1), or decode (2)?"
  choice <- getLine
  case choice of
    "1" -> encodeVigenere
    "2" -> decodeVigenere

encodeVigenere :: IO ()
encodeVigenere = do
  putStrLn "What do you want to encode?"
  string <- getLine
  putStrLn "With what keyword?"
  keyword <- getLine
  putStr "Encoded: "
  putStrLn $ vigenere keyword string

decodeVigenere :: IO ()
decodeVigenere = do
  putStrLn "What do you want to decode?"
  string <- getLine
  putStrLn "With what keyword?"
  keyword <- getLine
  putStr "Decoded: "
  putStrLn $ unVigenere keyword string


main :: IO ()
main = do
  putStrLn "Select a cipher:"
  putStrLn "1) caesar"
  putStrLn "2) vigenere"
  cipher <- getLine
  case cipher of
    "1" -> promptCaesar
    "2" -> promptVigenere
    _   -> putStrLn "Choose between 1 or 2."
