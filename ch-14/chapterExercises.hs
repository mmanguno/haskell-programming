module ChapterExercises where

import Data.Char ( toUpper, toLower )
import Data.List ( sort )

import Test.QuickCheck

import Ciphers
-- Using QuickCheck
-- see (QC.hs)


-- Failure

-- It doesn't hold because it's using floating point numbers which,
-- when very small, are inaccurate. The identity won't be exact,
-- only very close.
square :: Num a => a -> a
square x = x * x

squareIdentity :: (Eq a, Floating a) => a -> Bool
squareIdentity x = (square . sqrt) x == x

prop_squareIdentity :: Double -> Bool
prop_squareIdentity = squareIdentity


-- Idempotence

twice f = f . f

fourTimes = twice . twice

capitalizeWord :: String -> String
capitalizeWord ""     = ""
capitalizeWord (x:xs) = toUpper x : xs

-- 1.
prop_capIdem :: String -> Bool
prop_capIdem x =
  (capitalizeWord x == twice capitalizeWord x)
  && (capitalizeWord x == fourTimes capitalizeWord x)

-- 2.
prop_sortIdem :: [Int] -> Bool
prop_sortIdem x =
  (sort x == twice sort x)
  && (sort x == fourTimes sort x)


-- Make a Gen random generator for the datatype

data Fool =
    Fulse
  | Frue
  deriving (Eq, Show)

-- 1.

genFoolEq :: Gen Fool
genFoolEq = elements [Fulse, Frue]

genFoolFulsey :: Gen Fool
genFoolFulsey = frequency [ (2, return Fulse)
                          , (1, return Frue)]


-- Hangman testing
-- see hangman.hs

-- Validating ciphers
-- (only works on lowercased strings)
lowerAlphanumeric :: Gen String
lowerAlphanumeric = listOf (elements ['a'..'z'])

prop_vigenere :: String -> String -> Bool
prop_vigenere s k = ((unVigenere k) . (vigenere k)) s == s

prop_caesar :: String -> Int -> Bool
prop_caesar s i = ((unCaesar i) . (caesar i)) s == s

main :: IO ()
main = do
  putStrLn "Square Identity:"
  quickCheck prop_squareIdentity
  putStrLn "Idempotence"
  quickCheck prop_capIdem
  quickCheck prop_sortIdem
  putStrLn "Ciphers"
  quickCheck $ forAll lowerAlphanumeric prop_vigenere
  quickCheck $ forAll lowerAlphanumeric prop_caesar
