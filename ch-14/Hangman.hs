module Hangman where

import Data.List ( intersperse )

import Test.Hspec

data Puzzle =
  Puzzle String [Maybe Char] [Char]
  deriving Eq

instance Show Puzzle where
  show (Puzzle _ discovered guessed) =
    (intersperse ' ' $ fmap renderPuzzleChar discovered)
    ++ " Guessed so far: " ++ guessed


charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle s _ _) c = c `elem` s


alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ _ g) c = c `elem` g


renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar Nothing  = '_'
renderPuzzleChar (Just c) = c


fillInCharacter :: Puzzle -> Char -> Puzzle
fillInCharacter (Puzzle word
                 filledInSoFar s) c =
  Puzzle word newFilledInSoFar (c : s)
  where zipper guessed wordChar guessChar =
          if wordChar == guessed
          then Just wordChar
          else guessChar
        newFilledInSoFar =
          let zd = (zipper c)
          in zipWith zd word filledInSoFar


handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord puzzle guess
       , alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You already guessed that\
               \ character, pick\
               \ something else!"
      return puzzle
    (True, _) -> do
      putStrLn "This character was in the\
               \ word, filling in the\
               \ word accordingly"
      return (fillInCharacter puzzle guess)
    (False, _) -> do
      putStrLn "This character wasn't in\
               \ the word, try again."
      return (fillInCharacter puzzle guess)


main :: IO ()
main = hspec $ do
  describe "fillInCharacter" $ do
    it "fills in correct characters" $ do
      fillInCharacter (Puzzle "hello" (take 5 $ repeat Nothing) []) 'h'
        `shouldBe`    (Puzzle "hello" (Just 'h':(take 4 $ repeat Nothing)) ['h'])
    it "doesn't fill incorrect characters" $ do
      fillInCharacter (Puzzle "hello" (take 5 $ repeat Nothing) []) 'j'
        `shouldBe`    (Puzzle "hello" (take 5 $ repeat Nothing) ['j'])


  describe "handleGuess" $ do
    it "handles correct guesses" $ do
      handleGuess  (Puzzle "hello" (take 5 $ repeat Nothing) []) 'h'
        `shouldReturn` (Puzzle "hello" (Just 'h':(take 4 $ repeat Nothing)) ['h'])
    it "handles incorrect guesses" $ do
      handleGuess  (Puzzle "hello" (take 5 $ repeat Nothing) []) 'j'
        `shouldReturn` (Puzzle "hello" (take 5 $ repeat Nothing) ['j'])
    it "handles repeat incorrect guesses" $ do
      handleGuess  (Puzzle "hello" (take 5 $ repeat Nothing) ['j']) 'j'
        `shouldReturn` (Puzzle "hello" (take 5 $ repeat Nothing) ['j'])
    it "handles repeat correct guesses" $ do
      handleGuess  (Puzzle "hello" (Just 'h':(take 4 $ repeat Nothing)) ['h']) 'h'
        `shouldReturn` (Puzzle "hello" (Just 'h':(take 4 $ repeat Nothing)) ['h'])
