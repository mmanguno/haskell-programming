module QC where

import Data.List (sort)
import Test.QuickCheck

-- 1.
half :: Fractional a => a -> a
half x = x / 2

halfIdentity :: Fractional a => a -> a
halfIdentity = (*2) . half

prop_halfIdentity :: Double -> Bool
prop_halfIdentity x = (halfIdentity x) == x


-- 2.
listOrdered :: (Ord a) => [a] -> Bool
listOrdered xs =
  snd $ foldr go (Nothing, True) xs
  where go _ status@(_, False) = status
        go y (Nothing, t) = (Just y, t)
        go y (Just x, _) = (Just y, x >= y)

prop_listOrdered :: [Double] -> Bool
prop_listOrdered = listOrdered . sort


-- 3.
plusAssociative :: (Eq a, Num a) => a -> a -> a -> Bool
plusAssociative x y z = x + (y + z) == (x + y) + z

plusCommutative :: (Eq a, Num a) => a -> a -> Bool
plusCommutative x y = x + y == y + x

prop_plusAssociative :: Int -> Int -> Int -> Bool
prop_plusAssociative = plusAssociative

prop_plusCommutative :: Int -> Int -> Bool
prop_plusCommutative = plusCommutative


-- 4.
multAssociative :: (Eq a, Num a) => a -> a -> a -> Bool
multAssociative x y z = x * (y * z) == (x * y) * z

multCommutative :: (Eq a, Num a) => a -> a -> Bool
multCommutative x y = x * y == y * x

prop_multAssociative :: Int -> Int -> Int -> Bool
prop_multAssociative = multAssociative

prop_multCommutative :: Int -> Int -> Bool
prop_multCommutative = multCommutative


-- 5.
nonzeroGen :: Gen Int
nonzeroGen = arbitrary `suchThat` (/=0)

--or...
newtype Nonzero = Nonzero Integer deriving (Eq, Show)

instance Arbitrary Nonzero where
  arbitrary = fmap Nonzero (arbitrary `suchThat` (/=0))

-- how to do multiple forAlls
-- https://stackoverflow.com/a/45520043
prop_quotRem :: Property
prop_quotRem = forAll nonzeroGen $ \x ->
                 forAll nonzeroGen $ \y ->
                   (quot x y)*y + (rem x y) == x

prop_divMod :: Property
prop_divMod = forAll nonzeroGen $ \x ->
                 forAll nonzeroGen $ \y ->
                   (div x y)*y + (mod x y) == x


-- 6.
prop_expAssoc :: Int -> Int -> Int -> Bool
prop_expAssoc x y z = x ^ (y ^ z) == (x ^ y) ^ z

prop_expComm :: Int -> Int -> Bool
prop_expComm x y = x ^ y == y ^ x


-- 7.
reverse' :: Eq a => [a] -> Bool
reverse' xs = ((reverse . reverse) xs) == xs

prop_reverse :: [Int] -> Bool
prop_reverse = reverse'


-- 8.
prop_apply :: Fun Int Int -> Int -> Bool
prop_apply f a = (f' $ a) == (f' a)
  where f' = applyFun f

prop_compose :: Fun Int Int -> Fun Int Int -> Int -> Bool
prop_compose f g x = ((f' . g') x) == (f' (g' x))
  where f' = applyFun f
        g' = applyFun g

-- 9.

foldEq :: Eq a => [a] -> [a] -> Bool
foldEq xs ys = (foldr (:)) xs ys == (++) xs ys

prop_foldEq :: [Int] -> [Int] -> Bool
prop_foldEq = foldEq


-- 10.

prop_length :: Int -> [Int] -> Bool
prop_length n xs = length (take n xs) == n

-- 11.

prop_roundTrip :: Int -> Bool
prop_roundTrip x = (read (show x)) == x

main :: IO ()
main = do
  putStrLn "prop_halfIdentity:"
  quickCheck prop_halfIdentity
  putStrLn "prop_listOrdered:"
  quickCheck prop_listOrdered
  putStrLn "prop_plusAssociative:"
  quickCheck prop_plusAssociative
  putStrLn "prop_plusCommutative:"
  quickCheck prop_plusCommutative
  putStrLn "prop_plusAssociative:"
  quickCheck prop_multAssociative
  putStrLn "prop_multCommutative:"
  quickCheck prop_multCommutative
  putStrLn "prop_quotRem:"
  quickCheck prop_quotRem
  putStrLn "prop_divMod:"
  quickCheck prop_divMod
  putStrLn "prop_expAssoc:"
  quickCheck prop_expAssoc
  putStrLn "prop_expComm:"
  quickCheck prop_expComm
  putStrLn "prop_reverse:"
  quickCheck prop_reverse
  putStrLn "prop_apply:"
  quickCheck prop_apply
  putStrLn "prop_compose:"
  quickCheck prop_compose
  putStrLn "prop_foldEq:"
  quickCheck prop_foldEq
  putStrLn "prop_length:"
  quickCheck prop_length
  putStrLn "prop_roundTrip:"
  quickCheck prop_roundTrip
