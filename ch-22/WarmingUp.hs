module WarmingUp where

import Data.Char

cap :: [Char] -> [Char]
cap xs = map toUpper xs

rev :: [Char] -> [Char]
rev xs = reverse xs

composed :: [Char] -> [Char]
composed = cap . rev

fmapped :: [Char] -> [Char]
fmapped = fmap cap rev

tupled :: [Char] -> ([Char], [Char])
tupled = do
  up <- cap
  re <- rev
  return (up, re)

tupled' :: [Char] -> ([Char], [Char])
tupled' = cap >>= (\up -> rev >>= (\rev -> return (up, rev)))

main :: IO ()
main = do
  putStrLn "composed:"
  putStrLn $ composed "Julie"
  putStrLn "fmapped:"
  putStrLn $ fmapped "Chris"
  putStrLn "tupled"
  putStrLn $ show . tupled $ "Julie"
  putStrLn "tupled'"
  putStrLn $ show . tupled' $ "Chris"
