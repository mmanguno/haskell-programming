module ReaderPractice where

import Control.Applicative
import Data.Maybe

x = [1, 2, 3]
y = [4, 5, 6]
z = [7, 8, 9]

lookup' :: Eq a => a -> [(a, b)] -> Maybe b
lookup' _ []           = Nothing
lookup' a ((a', b'):abs)
  | a == a'   = Just b'
  | otherwise = lookup' a abs

xs :: Maybe Integer
xs = lookup' 3 $ zip x y

ys :: Maybe Integer
ys = lookup' 6 $ zip y z

zs :: Maybe Integer
zs = lookup' 4 $ zip x y

z' :: Integer -> Maybe Integer
z' n = lookup' n $ zip x z


x1 :: Maybe (Integer, Integer)
x1 = Just (,) <*> xs <*> ys

x2 :: Maybe (Integer, Integer)
x2 = Just (,) <*> ys <*> zs

x3 :: Integer
   -> (Maybe Integer, Maybe Integer)
x3 = (,) <$> z' <*> z'  -- or, liftA2 (,) z' z'


uncurry' :: (a -> b -> c) -> (a, b) -> c
uncurry' f (a, b) = f a b

summed :: Num c => (c, c) -> c
summed (c, c') = c + c'

bolt :: Integer -> Bool
bolt = (<8)

fromMaybe' :: a -> Maybe a -> a
fromMaybe' d Nothing  = d
fromMaybe' _ (Just a) = a


sequA :: Integral a => a -> [Bool]
sequA m = sequenceA [(>3), (<8), even] m

s' = summed <$> ((,) <$> xs <*> ys)

main :: IO ()
main = do
  -- 1.
  print $ foldr (&&) True $ sequA 6
  -- 2.
  print $ sequA . fromMaybe 0 $ s'
  -- 3.
  print $ bolt . fromMaybe 0 $ ys
