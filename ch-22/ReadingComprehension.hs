{-# LANGUAGE InstanceSigs #-}

module ReadingComprehension where


newtype Reader r a =
  Reader { runReader :: r -> a }

instance Functor (Reader r) where
  fmap f (Reader ra) =
    Reader $ (f . ra)


myLiftA2 :: Applicative f =>
            (a -> b -> c)
         -> f a -> f b -> f c
myLiftA2 f fA fB = f <$> fA <*> fB

asks :: (r -> a) -> Reader r a
asks f = Reader f


instance Applicative (Reader r) where
  pure :: a -> Reader r a
  pure a = Reader $ const a

  (<*>) :: Reader r (a -> b)
        -> Reader r a
        -> Reader r b
  (Reader rab) <*> (Reader ra) =
    Reader $ rab <*> ra
    -- equivalent to
    -- Reader $ (\r -> (rab r) (ra r))
