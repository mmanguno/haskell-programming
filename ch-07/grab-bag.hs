
-- 1.
-- all of them are equivalent

mTh x y z = x * y * z

-- 2.
-- d) Num a => a -> a -> a

-- *Main> :t mTh
-- mTh :: Num a => a -> a -> a -> a
-- *Main> :t mTh 3
-- mTh 3 :: Num a => a -> a -> a

-- 3.

-- a.
addOneIfOdd n = case odd n of
  True -> f n
  False -> n
  where f = \n -> n + 1

-- b.
addFive = \x -> \y -> (if x > y then y else x) + 5

-- c.
mflip f x y = f y x
