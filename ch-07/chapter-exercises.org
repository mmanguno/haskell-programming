* Multiple choice

1. A polymorphic function *may resolve to values of different types,*
   *depending on inputs*.
2. Two functions named =f= and =g= have types =Char -> String= and
   =String -> [String]= respectively. The composed function =g . f=
   has the type *=Char -> [String]=*.
3. A function =f= has the type =Ord a => a -> a -> Bool= and we apply
   it to one numeric value. What is the type now?
   *=(Ord a, Num a) => a -> Bool=*.
4. A function with the type =(a -> b) -> c= *is a higher-order function*.
5. Given the following definition of =f=, what is the type of =f True=?
   #+begin_src haskell
   f :: a -> a
   f x = x
   #+end_src
   =f True :: Bool=


* Let's write code

** 1

*** a
#+begin_src haskell
tensDigit x = d
  where (tens, _) = x `divMod` 10
        (_, d)    = tens `divMod` 10
#+end_src


*** b
Yes:
#+begin_src haskell
tensDigit :: Integral b => b -> b
#+end_src

*** c
#+begin_src haskell
hunsD x = d
  where (hundreds, _) = x `divMod` 100
        (_, d)    = hundreds `divMod` 10
#+end_src




** 2

#+begin_src haskell
foldBool :: a -> a -> Bool -> a
foldBool x y bool =
  case bool of
    True  -> x
    False -> y
#+end_src

#+begin_src haskell
foldBool' :: a -> a -> Bool -> a
foldBool' x y bool
  | bool      = x
  | otherwise = y
#+end_src


** 3

#+begin_src haskell
g :: (a -> b) -> (a, c) -> (b, c)
g aToB (a, c) = (aToB a, c)
#+end_src


** 4

(see =arith4.hs=)
