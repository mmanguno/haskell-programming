
-- 1.
avgGrade :: (Fractional a, Ord a) => a -> Char
avgGrade x
  | otherwise = 'F'
  | y >= 0.9  = 'A'
  | y >= 0.8  = 'B'
  | y >= 0.7  = 'C'
  | y >= 0.59 = 'D'
  | y <  0.59 = 'F'
  where y = x / 100


-- 2.
avgGrade2 :: (Fractional a, Ord a) => a -> Char
avgGrade2 x
  | y >= 0.7  = 'C'
  | y >= 0.9  = 'A'
  | y >= 0.8  = 'B'
  | y >= 0.59 = 'D'
  | y <  0.59 = 'F'
  where y = x / 100


-- 3.
pal xs
  | xs == reverse xs = True
  | otherwise        = False

-- 4.
-- Any list.

-- 5.
-- pal :: (Eq a) => [a] -> Bool

-- 6.
numbers x
  | x < 0  = -1
  | x == 0 = 0
  | x > 0  = 1

-- 7.
-- (Ord a, Num a) => a

-- 8.
-- numbers :: (Ord a, Num a, Num b) => a -> b
