-- chapter exercises (pg. 60 - 64)

-- Parenthesization

-- 1)
-- 2 + (2 * 3) - 1

-- 2)
-- 10 ^ (1 + 1)

-- 3)
-- ((2 ^ 2) * (4 ^ 5)) + 1


-- Equivalent expressions

-- 1) yes.
eq1 = (1 + 1) == 2 -- => True

-- 2) yes.
eq2 = (10 ^ 2) == (10 + 9 * 10) -- => True

-- 3) no. arguments reversed
eq3 = (400 - 37) == ((-) 37 400) -- => False

-- 4) no. float division vs. integral division
-- eq4 = (100 `div` 3) == (100 / 3) -- => won't compile; ambiguous types
                                    -- compare 33 vs. 33.333333333333336

-- 5) no. order of operations altered
eq5 = (2 * 5 + 18) == (2 * (5 + 18)) -- => False


-- More fun with functions

-- z = 7
-- x = y ^ 2     -- 15 ^ 2  = 225
-- waxOn = x * 5 -- 225 * 5 = 1125
-- y = z + 8     -- 7 + 8   = 15

waxOn     = x * 5
  where z = 7
        y = z + 8
        x = y ^ 2

triple x = x * 3

waxOff x = triple x
