-- exercises: parentheses and association

-- 1) yes, changes (order of operations altered)
testEqual1 = (8 + 7 * 9) == ((8 + 7) * 9) -- False

-- 2) no change (parentheses same as order of operations)
perimeter1 x y = (x * 2) + (y * 2)
perimeter2 x y = x * 2 + y * 2
testEqual2 = (perimeter1 3 7) == (perimeter2 3 7) -- True

-- 3) yes, changes (order of operations altered)
f1 x = x / 2 + 9
f2 x = x / (2 + 9)
testEqual3 = (f1 5) == (f2 5)
