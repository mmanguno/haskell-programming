-- exercises: comprehension check (pg. 34-35)

-- 1)
half x = x / 2  -- let half x = x / 2
square x = x * x -- let square x = x * x

-- 2)
timesPi x = 3.14 * x

-- 3)
timesPiTwo x = pi * x
