{-# LANGUAGE FlexibleContexts #-}

module S where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

data S n a = S (n a) a deriving (Eq, Show)


instance ( Functor n
         , Arbitrary (n a)
         , Arbitrary a )
        => Arbitrary (S n a) where
  arbitrary = S <$> arbitrary <*> arbitrary


-- instance ( Applicative n
--          , Testable (n Property)
--          , EqProp a )
--         => EqProp (S n a) where
--   (S x y) =-= (S p q) =
--     (property $ (=-=) <$> x <*> p)
--     .&. (y =-= q)

-- ^ That makes everything fail in somewhat nonsensical ways.
-- e.g.
--
--     > main
--
--     functor:
--       identity: *** Failed! Falsifiable (after 9 tests and 2 shrinks):
--     S [2,-7,3,-7,2,0,-3] 4
--     LHS
--     1
--
--     > let s = S [2,-7,3,-7,2,0,-3] 4
--     > fmap id s == id s  -- should be False, according to Checkers
--     True
--
-- There doesn't seem to be anything wrong with that instance;
-- it looks as though it just pieces apart (=-=) into comparing
-- the a, then (n a).
--
-- The below works without issue, and all tests pass. Will ask about this.

instance (Eq a, Eq (n a)) => EqProp (S n a) where
  (=-=) = eq

instance Functor n => Functor (S n) where
  fmap f (S n a) = S (fmap f n) (f a)

instance Foldable n => Foldable (S n) where
  foldMap f (S n a) = (foldMap f n) <> (f a)

instance Traversable n
      => Traversable (S n) where
  traverse f (S n a) = S <$> (traverse f n) <*> (f a)


main = do
  let s :: S [] (Int, Int, [Int])
      s = undefined
  quickBatch $ functor s
  quickBatch $ traversable s
