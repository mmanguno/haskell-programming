module ChapterExercises where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes


-- 1.
newtype Identity a = Identity a
  deriving (Eq, Ord, Show)

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Foldable Identity where
  foldMap f (Identity a) = f a

instance Traversable Identity where
  traverse f (Identity a) = Identity <$> f a

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = Identity <$> arbitrary

instance Eq a => EqProp (Identity a) where
  (=-=) = eq


-- 2.
newtype Constant a b =
  Constant { getConstant :: a }
  deriving (Eq, Show)

instance Functor (Constant a) where
  fmap _ (Constant a) = (Constant a)

instance Foldable (Constant a) where
  foldMap f (Constant a) = mempty

instance Traversable (Constant a) where
  traverse _ (Constant a) = pure (Constant a)

instance Arbitrary a => Arbitrary (Constant a b) where
  arbitrary = Constant <$> arbitrary

instance Eq a => EqProp (Constant a b) where
  (=-=) = eq


-- 3.
data Optional a =
    Nada
  | Yep a
  deriving (Eq, Show)

instance Functor Optional where
  fmap _ Nada    = Nada
  fmap f (Yep a) = Yep $ f a

instance Foldable Optional where
  foldMap _ Nada    = mempty
  foldMap f (Yep a) = f a

instance Traversable Optional where
  traverse _ Nada    = pure Nada
  traverse f (Yep a) = Yep <$> f a

instance Arbitrary a => Arbitrary (Optional a) where
  arbitrary = frequency [ (1, pure Nada)
                        , (2, Yep <$> arbitrary) ]

instance Eq a => EqProp (Optional a) where
  (=-=) = eq


-- 4.
data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)

instance Functor List where
  fmap _ Nil         = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Foldable List where
  foldMap _ Nil         = mempty
  foldMap f (Cons x xs) = (f x) <> (foldMap f xs)

instance Traversable List where
  traverse _ Nil         = pure Nil
  traverse f (Cons x xs) = (Cons <$> f x) <*> (traverse f xs)

instance Arbitrary a => Arbitrary (List a) where
  arbitrary = do
    a <- arbitrary
    l <- arbitrary
    frequency [ (1, return (Cons a l))
              , (2, return (Cons a Nil)) ]

instance Eq a => EqProp (List a) where
  (=-=) = eq

-- 5.
data Three a b c =
  Three a b c
  deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three a b c) = Three a b (f c)

instance Foldable (Three a b) where
  foldMap f (Three a b c) = f c

instance Traversable (Three a b) where
  traverse f (Three a b c) = (Three a b) <$> (f c)

instance (Arbitrary a, Arbitrary b, Arbitrary c)
       => Arbitrary (Three a b c) where
  arbitrary = Three <$> arbitrary <*> arbitrary <*> arbitrary

instance (Eq a, Eq b, Eq c) => EqProp (Three a b c) where
  (=-=) = eq


-- 6.
data Pair a b =
  Pair a b
  deriving (Eq, Show)

instance Functor (Pair a) where
  fmap f (Pair a b) = Pair a (f b)

instance Foldable (Pair a) where
  foldMap f (Pair a b) = f b

instance Traversable (Pair a) where
  traverse f (Pair a b) = (Pair a) <$> (f b)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Pair a b) where
  arbitrary = Pair <$> arbitrary <*> arbitrary

instance (Eq a, Eq b) => EqProp (Pair a b) where
  (=-=) = eq


-- 7.
data Big a b =
  Big a b b
  deriving (Eq, Show)

instance Functor (Big a) where
  fmap f (Big a b b') = Big a (f b) (f b')

instance Foldable (Big a) where
  foldMap f (Big a b b') = (f b) <> (f b')

instance Traversable (Big a) where
  traverse f (Big a b b') = (Big a) <$> (f b) <*> (f b')

instance (Arbitrary a, Arbitrary b) => Arbitrary (Big a b) where
  arbitrary = Big <$> arbitrary <*> arbitrary <*> arbitrary

instance (Eq a, Eq b) => EqProp (Big a b) where
  (=-=) = eq


-- 8.
data Bigger a b =
  Bigger a b b b
  deriving (Eq, Show)

instance Functor (Bigger a) where
  fmap f (Bigger a b b' b'') = Bigger a (f b) (f b') (f b'')

instance Foldable (Bigger a) where
  foldMap f (Bigger a b b' b'')
    = (f b) <> (f b') <> (f b'')

instance Traversable (Bigger a) where
  traverse f (Bigger a b b' b'')
    = (Bigger a) <$> (f b) <*> (f b') <*> (f b'')

instance (Arbitrary a, Arbitrary b) => Arbitrary (Bigger a b) where
  arbitrary = Bigger
    <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

instance (Eq a, Eq b) => EqProp (Bigger a b) where
  (=-=) = eq

-- 9.
-- see S.hs

-- 10.
-- see Tree.hs


main = do
  let identity :: Identity (Int, Int, [Int])
      identity = undefined
      constant :: Constant (Int, Int, [Int]) (Int, Int, [Int])
      constant = undefined
      optional :: Optional (Int, Int, [Int])
      optional = undefined
      list :: List (Int, Int, [Int])
      list = undefined
      three :: Three (Int, Int, [Int]) (Int, Int, [Int]) (Int, Int, [Int])
      three = undefined
      pair :: Pair (Int, Int, [Int]) (Int, Int, [Int])
      pair = undefined
      big :: Big (Int, Int, [Int]) (Int, Int, [Int])
      big = undefined
      bigger :: Bigger (Int, Int, [Int]) (Int, Int, [Int])
      bigger = undefined
  quickBatch (traversable identity)
  quickBatch (traversable constant)
  quickBatch (traversable optional)
  quickBatch (traversable list)
  quickBatch (traversable three)
  quickBatch (traversable big)
  quickBatch (traversable bigger)
