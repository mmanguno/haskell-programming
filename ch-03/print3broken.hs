module Print3Broken where

printSecond :: IO ()
printSecond = do
  putStrLn "Yarrrrr"

main :: IO ()
main = do
  putStrLn greeting
  printSecond
  where greeting = "Yarrrrr"
