-- Reading syntax

-- 1)
-- a. yes
-- b. no. (++) [1, 2, 3] [4, 5, 6]
-- c. yes
-- d. no. ["hello" ++ " world"]
-- e. no. "hello" !! 4
-- f. yes
-- g. no. take 4 "lovely"
-- h. yes.

-- 2)
-- a -> d
-- b -> c
-- c -> e
-- d -> a
-- e -> b


-- Building functions

-- 1/2.
dropOne :: [a] -> [a]
dropOne x = drop 1 x

-- a.
addBang :: [Char] -> [Char]
addBang x = x ++ "!"


-- b.
fourthItem :: [a] -> [a]
fourthItem x = drop 4 $ take 5 x

-- c.
removeFirstNine :: [a] -> [a]
removeFirstNine x = drop 9 x


-- 3.
thirdLetter :: [Char] -> Char
thirdLetter x = x !! 2

-- 4.
letterIndex :: Int -> Char
letterIndex x = string !! x
  where string = "Curry is awesome!"

-- 5.
rvrs :: [Char]
rvrs = awesome ++ " " ++ is ++ " " ++ curry
  where string  = "Curry is awesome"
        curry   = take 5 string
        is      = drop 6 $ take 8 string
        awesome = drop 9 string

-- 6.
-- see reverse.hs
