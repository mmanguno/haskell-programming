-- 1)
-- yes, y is in scope for z (defined before it)

-- 2)
-- no, h isn't defined anywhere

-- 3)
-- no. d is not defined in the scope of r


-- area d = pi * (r * r)
-- r = d / 2

-- exercises/scope.hs:10:5: error: Variable not in scope: d
--   |
--10 | r = d / 2
--   |


-- 4) yes, d is in scope of r

area d = pi * (r * r)
  where r = d / 2
