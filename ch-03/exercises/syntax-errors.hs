-- 1)
-- no, ++ needs to be surrounded in parenthesis to be used in prefix notation

-- 2)
-- no; single quotes indicate a Char, not [Char] (i.e. String).

-- 3)
-- yes. concat is prefix notation, and it's argument is a [[Char]]
