module LibraryFunction where

import Data.Monoid ( Any(Any), getAny
                   , Product(Product), getProduct
                   , Sum(Sum), getSum )


-- 1.

sum' :: (Foldable t, Num a) => t a -> a
sum' = getSum . foldMap Sum


-- 2.
product' :: (Foldable t, Num a) => t a -> a
product' = getProduct . foldMap Product


-- 3.
elem' :: (Foldable t, Eq a)
      => a -> t a -> Bool
elem' e = getAny . foldMap (Any . (==e))

-- 4.

data Minimum a =
    Minimum { getMinimum :: a }
  | Greatest
  deriving (Eq, Show)

instance Ord a => Semigroup (Minimum a) where
  Greatest  <> Minimum a  = Minimum a
  Minimum a <> Greatest   = Minimum a
  Minimum a <> Minimum a' = if a < a' then Minimum a else Minimum a'

instance Ord a => Monoid (Minimum a) where
  mempty = Greatest

minimum' :: (Foldable t, Ord a)
         => t a -> Maybe a
minimum' t
  | null t    = Nothing
  | otherwise = Just . getMinimum . foldMap Minimum $ t


-- 5.

data Maximum a =
    Maximum { getMaximum :: a }
  | Least
  deriving (Eq, Show)

instance Ord a => Semigroup (Maximum a) where
  Least     <> Maximum a  = Maximum a
  Maximum a <> Least      = Maximum a
  Maximum a <> Maximum a' = if a > a' then Maximum a else Maximum a'

instance Ord a => Monoid (Maximum a) where
  mempty = Least

maximum' :: (Foldable t, Ord a)
         => t a -> Maybe a
maximum' t
  | null t    = Nothing
  | otherwise = Just . getMaximum . foldMap Maximum $ t


-- 6.
null' :: (Foldable t) => t a -> Bool
null' t = length == 0
  where length = foldr ((+) . const 1) 0 t


-- 7.
length' :: (Foldable t) => t a -> Int
length' = foldr ((+) . const 1) 0


-- 8.
toList' :: (Foldable t) => t a -> [a]
toList' t = foldr (:) [] t


-- 9.
fold' :: (Foldable t, Monoid m) => t m -> m
fold' = foldMap id


-- 10.
foldMap' :: (Foldable t, Monoid m)
         => (a -> m) -> t a -> m
foldMap' f t = foldr ((<>) . f) mempty t
