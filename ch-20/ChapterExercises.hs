module ChapterExercises where


-- 1.

data Constant a b =
  Constant b
  deriving (Eq, Show)


instance Foldable (Constant a) where
  foldr f v (Constant b) = f b v


-- 2.

data Two a b =
  Two a b
  deriving (Eq, Show)

instance Foldable (Two a) where
  foldr f v (Two a b) = f b v


-- 3.

data Three a b c =
  Three a b c
  deriving (Eq, Show)

instance Foldable (Three a b) where
  foldMap f (Three a b c) = f c


-- 4.

data Three' a b =
  Three' a b b
  deriving (Eq, Show)

instance Foldable (Three' a) where
  foldMap f (Three' a b b') = (f b) <> (f b')


-- 5.

data Four' a b =
  Four' a b b b
  deriving (Eq, Show)

instance Foldable (Four' a) where
  foldMap f (Four' a b b' b'') = (f b) <> (f b') <> (f b'')


-- 6.

-- Must be applicative in order to use pure.
-- Key logic: mempty <> (pure a) = pure a.
-- Replace all non-matches with mempties; wrap good values
-- in pure.

filterF :: ( Applicative f
           , Foldable t
           , Monoid (f a))
        => (a -> Bool) -> t a -> f a
filterF p t = foldMap filter' t
  where
    filter' a = if (p a) then pure a else mempty
