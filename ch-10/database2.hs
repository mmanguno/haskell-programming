-- doing again, since finished the other in a group

module Database where

import Data.Time

data DatabaseItem = DbString String
                  | DbNumber Integer
                  | DbDate UTCTime
                  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [ DbDate (UTCTime
            (fromGregorian 1911 5 1)
            (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, world!"
  , DbDate (UTCTime
            (fromGregorian 1921 5 1)
            (secondsToDiffTime 34123))
  ]

filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate = foldr f []
  where
    f (DbDate a) acc  = a:acc
    f _ acc           = acc


filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber = foldr f []
  where
    f (DbNumber a) acc = a:acc
    f _ acc            = acc


mostRecent :: [DatabaseItem] -> Maybe UTCTime
mostRecent = foldr f Nothing
  where
    f (DbDate a) (Just acc) = Just (if a < acc then a else acc)
    f (DbDate a) Nothing    = Just a
    f _ acc                 = acc


sumDb :: [DatabaseItem] -> Maybe Integer
sumDb = foldr f Nothing
  where
    f (DbNumber a) (Just acc) = Just (a + acc)
    f (DbNumber a) Nothing    = Just a
    f _ acc                   = acc


avgDb :: [DatabaseItem] -> Maybe Double
avgDb l
  | count == 0 = Nothing
  | otherwise  = Just ((fromIntegral sum) / (fromIntegral count))
  where
    (sum, count) = foldr f (0, 0) l
    f (DbNumber a) (s, c) = (s + a, c + 1)
    f  _ acc              = acc
