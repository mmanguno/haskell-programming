-- doing again, since finished the other in a group

module ChapterExercises where

import Data.List

-- Warm-up and review

-- 1.
stops = "pbtdkg"
vowels = "aeiou"

-- a.
combinations = [a : b : c : [] | a <- stops, b <- vowels, c <- stops]

-- b.
startsWithP = ['p' : a : b : [] | a <- vowels, b <- stops]

-- c.
nouns = ["cat", "dog", "aardvark", "endo"]
verbs = ["found", "saw", "bit" , "applied"]

sentences = [n ++ " " ++  v  ++ " " ++ n' | n <- nouns,
                                            v <- verbs,
                                            n' <- nouns]


-- 2.

-- It divides the sum of the lengths of the words in a String, by the number
-- of words in the String. I.e., it returns the average word length.

-- Integral a => String -> a

seekritFunc x =
  div (sum (map length (words x)))
      (length (words x))

-- 3.

seekritFunc' x =
  (/) ((fromIntegral . sum . map length . words) x)
      ((fromIntegral . length . words) x)


-- Rewriting functions using folds

-- 1.

myOr :: [Bool] -> Bool
myOr = foldr (||) False


-- 2
myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr (\v acc -> f v) False


-- 3.
myElem :: Eq a => a -> [a] -> Bool
myElem e = foldr (\v acc -> v == e) False

myElem' :: Eq a => a -> [a] -> Bool
myElem' e = myAny (==e)

-- 4.
myReverse :: [a] -> [a]
myReverse = foldl' (flip (:)) []


-- 5.

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr (\v acc -> (f v) : acc) []

-- 6.

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f = foldr fold []
  where
    fold v acc = if f v then v : acc else acc


-- 7.
squish :: [[a]] -> [a]
squish = foldr (++) []


-- 8.
squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f = foldr fold []
  where
    fold v acc = f v ++ acc

-- 9.
squishAgain :: [[a]] -> [a]
squishAgain = squishMap id


-- 10.
myMaximumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
myMaximumBy f [] = Nothing
myMaximumBy f l  = Just $ foldl' fold (head l) l
  where
    fold v acc
      | f v acc == GT = v
      | otherwise     = acc


-- 11.
myMinimumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
myMinimumBy f [] = Nothing
myMinimumBy f l  = Just $ foldl' fold (head l) l
  where
    fold v acc
      | f v acc == LT = v
      | otherwise     = acc
