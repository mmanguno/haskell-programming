module Scans where

fibs = 1 : scanl (+) 1 fibs
fibsN = (fibs !!)

-- 1.
twentyFibs = take 20 $ 1:(scanl (+) 1 twentyFibs)


-- 2.
lessThanHundredFibs = takeWhile (< 100) $ 1:(scanl (+) 1 lessThanHundredFibs)


-- 3.
factorial = 1 : scanl (*) 1 [1..]
