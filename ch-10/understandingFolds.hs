module UnderstandFolds where

-- 1.
-- b) and c)

b = foldr (*) 1 [1..5] == foldl (flip (*)) 1 [1..5]
c = foldr (*) 1 [1..5] == foldl (*) 1 [1..5]

-- 2.

two = foldl (flip (*)) 1 [1..3]

-- flip (*) == (*) because commutitivity, so just notating it as that

-- = foldl (flip (*)) (1 * 1) [2, 3]
-- = foldl (flip (*)) ((1 * 1) * 2) [3]
-- = foldl (flip (*)) (((1 * 1) * 2) * 3) []
-- = (((1 * 1) * 2) * 3)
-- = ((1 * 2) * 3)
-- = (2 * 3)
-- = 6

-- 3.
-- c) foldr, but not foldl, associates to te right

-- 4.
-- a) reduce structure

-- 5.
fiveA = foldr (++) [] ["woot", "WOOT", "woot"]
fiveB = foldr max ' ' "fear is the little death"
fiveC = foldr (&&) True [False, False]
fiveD = foldr (||) True [False, False]
-- no, it will always return true. this is because, at the end, it always
-- (||)s a True. It would evaluate to: (False || (False || True))

fiveE = foldl (flip ((++) . show)) "" [1..5]

fiveF = foldr (flip (const)) 'a' [1..5] -- must return a Char
-- foldr const 'a' [1,2,3,4,5]
-- = const 1 (const 2 (const 3 (const 4 (const 5 'a')))) = 1 /= Char

-- it has to be a -> b -> b so that, when recursing back up, it can
-- successively supply 'b's back to the second argument of the function.

fiveG = foldl const 0 "burritos"

fiveI = foldl const 'z' [1..5]
