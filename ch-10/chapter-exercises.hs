module ChapterExerices where

import Data.List

myOr :: [Bool] -> Bool
myOr = foldr (||) False

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr (\a acc -> (f a) || acc) False

myAny' :: (a -> Bool) -> [a] -> Bool
myAny' f = foldr ((||) . f) False

myElem :: Eq a => a -> [a] -> Bool
myElem e = foldr (\a acc -> e == a || acc) False

myElem' :: Eq a => a -> [a] -> Bool
myElem' z = myAny' (z==)

myReverse :: [a] -> [a]
myReverse = foldl' (\acc a -> a:acc) []

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr (\v acc -> f v : acc) []

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f = foldr (\v acc -> if f v then v:acc else acc) []

squish :: [[a]] -> [a]
squish = foldr (\v acc -> v ++ acc) []

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f = foldr (\v acc -> f v ++ acc) []

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
myMaximumBy c = foldr folding Nothing
  where
    folding v = maybe (Just v) (\v' -> Just (pick v v'))
    pick l r
      | c l r == GT  = l
      | otherwise    = r


myMinimumBy :: (a -> a -> Ordering) -> [a] -> Maybe a
myMinimumBy c = foldr folding Nothing
  where
    folding v = maybe (Just v) (\v' -> Just (pick v v'))
    pick l r
      | c l r == LT  = l
      | otherwise    = r
