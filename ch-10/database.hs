module Database where

import Data.Time

data DatabaseItem = DbString String
                  | DbNumber Integer
                  | DbDate   UTCTime
                  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [ DbDate (UTCTime
            (fromGregorian 1911 5 1)
            (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, world!"
  , DbDate (UTCTime
            (fromGregorian 1921 5 1)
            (secondsToDiffTime 34123))
  ]



-- 1.
filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate db = foldr f [] db
  where
    f (DbDate t) acc = t : acc
    f _ acc          = acc


-- 2.
filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber db = foldr f [] db
  where
    f (DbNumber i) acc = i : acc
    f _ acc            = acc


-- 3.
-- using maybe since an empty list has no max
mostRecent :: [DatabaseItem] -> Maybe UTCTime
mostRecent db
  | null dates = Nothing
  | otherwise  = Just $ foldr (\a b -> max a b) identity dates
  where
    dates = filterDbDate db
    identity = head dates  -- take a date, it will be identity


-- 4.
sumDb :: [DatabaseItem] -> Integer
sumDb = foldr f 0
  where
    f (DbNumber i) acc = i + acc
    f _ acc            = acc


-- 5.
avgDb :: [DatabaseItem] -> Double
avgDb = doAvg . foldr f (0, 0)
  where
    f (DbNumber i) (total, count) = (total + i, count + i)
    f _ acc                       = acc
    doAvg (total, count) = ((fromInteger total) / (fromInteger count))
