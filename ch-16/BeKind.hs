module BeKind where

-- 1.
-- a :: *

-- 2.
-- b :: *
-- T :: * -> * -> *

-- 3.
-- c :: * -> * -> *
