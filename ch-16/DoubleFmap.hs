module DoubleFmap where

--(.) :: (b -> c) -> (a -> b) -> a -> c
--       fmap        fmap
--fmap :: Functor f => (m -> n) -> f m -> f n
--fmap :: Functor g => (x -> y) -> g x -> g y


--(.) :: (Functor f, Functor g)
--    => ((g x -> g y) -> (f (g x) -> f (g y)))) -- (b -> c)
--    -> ((x -> y) -> (g x -> g y))              -- (a -> b)
--    -> ((x -> y) -> f (g x) -> f (g y))        -- (a -> c)
