{-# LANGUAGE FlexibleInstances #-}

module ChapterExercises where

-- Determine if a valid Functor can be written.

-- 1.
-- No. kind is *.

-- 2.
-- Yes. kind is * -> *.

-- 3.
-- Yes. kind is * -> *.

-- 4.
-- Yes. kind is * -> *.

-- 5.
-- No, kind is *.


-- Rearrange the arguments to the type constructor of the datatype
-- so the Functor instance works.

-- 1.
data Sum a b =
    First b
  | Second a

instance Functor (Sum e) where
  fmap f (First a) = First (f a)
  fmap f (Second b) = Second b


-- 2.
data Company a b c =
    DeepBlue a b
  | Something c


instance Functor (Company e e') where
  fmap f (Something b)  = Something (f b)
  fmap _ (DeepBlue a c) = DeepBlue a c


-- 3.
data More a b =
    L b a b
  | R a b a
  deriving (Eq, Show)

instance Functor (More x) where
  fmap f (L a b a') = L (f a) b     (f a')
  fmap f (R b a b') = R b     (f a) b'


-- Write Functor instances for the following datatypes.

-- 1.
data Quant a b =
    Finance
  | Desk a
  | Bloor b
  deriving (Eq, Show)

instance Functor (Quant a) where
  fmap f Finance   = Finance
  fmap f (Desk a)  = Desk a
  fmap f (Bloor b) = Bloor $ f b


-- 2.
data K a b =
  K a
  deriving (Eq, Show)

instance Functor (K a) where
  fmap _ (K a) = K a


-- 3.
newtype Flip f a b =
  Flip (f b a)
  deriving (Eq, Show)

newtype K' a b = K' a deriving (Eq, Show)

instance Functor (Flip K' a) where
  fmap f (Flip (K' a)) = Flip $ K' (f a)


-- 4.
data EvilGoateeConst a b =
  GoatyConst b
  deriving (Eq, Show)

instance Functor (EvilGoateeConst a) where
  fmap f (GoatyConst b) = GoatyConst $ f b


-- 5.
data LiftItOut f a =
  LiftItOut (f a)
  deriving (Eq, Show)

instance Functor f => Functor (LiftItOut f) where
  fmap f (LiftItOut funcOfA) = LiftItOut $ fmap f funcOfA

-- 6.
data Parappa f g a =
  DaWrappa (f a) (g a)
  deriving (Eq, Show)

instance (Functor f, Functor g) => Functor (Parappa f g) where
  fmap f (DaWrappa fOfA gOfA) = DaWrappa (fmap f fOfA) (fmap f gOfA)


--  7.
data IgnoreOne f g a b =
  IgnoreSomething (f a) (g b)
  deriving (Eq, Show)

instance (Functor g) => Functor (IgnoreOne f g a) where
  fmap f (IgnoreSomething fOfA gOfB) = IgnoreSomething fOfA (fmap f gOfB)


-- 8.
data Notorious g o a t =
  Notorious (g o) (g a) (g t)
  deriving (Eq, Show)

instance Functor g => Functor (Notorious g o a) where
  fmap f (Notorious gOfO gOfA gOfT) = Notorious gOfO gOfA (fmap f gOfT)


-- 9.
data List a =
    Nil
  | Cons a (List a)
  deriving (Eq, Show)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons x xs) = (Cons (f x) (fmap f xs))


-- 10.
data GoatLord a =
    NoGoat
  | OneGoat a
  | MoreGoats (GoatLord a)
              (GoatLord a)
              (GoatLord a)
  deriving (Eq, Show)


instance Functor GoatLord where
  fmap _ NoGoat               = NoGoat
  fmap f (OneGoat a)          = OneGoat (f a)
  fmap f (MoreGoats g g' g'') = MoreGoats (fmap f g) (fmap f g') (fmap f g'')


-- 11.
data TalkToMe a =
    Halt
  | Print String a
  | Read (String -> a)

-- There is an instance of Functor for (->). We'll use that.

instance Functor TalkToMe where
  fmap _ Halt = Halt
  fmap f (Print s a) = Print s (f a)
  fmap f (Read f') = (Read (fmap f f'))
