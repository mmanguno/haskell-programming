module InstancesOfFunc where

import Test.QuickCheck
import FunctorTest


type IntToInt = Fun Int Int

-- 1.
newtype Identity a = Identity a deriving (Eq, Show)

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return $ Identity a

type IdentityFC = Identity Int -> IntToInt -> IntToInt -> Bool


-- 2.
data Pair a = Pair a a deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair a a') = Pair (f a) (f a')


instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = do
    a <- arbitrary
    a' <- arbitrary
    return (Pair a a')

type PairFC = Pair Int -> IntToInt -> IntToInt -> Bool


-- 3.
data Two a b = Two a b deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two a b) = (Two a) $ f b

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return (Two a b)

type TwoFC = Two Int Int -> IntToInt -> IntToInt -> Bool

-- 4.
data Three a b c = Three a b c deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three a b c) = (Three a b) (f c)

instance (Arbitrary a, Arbitrary b, Arbitrary c) =>
         Arbitrary (Three a b c) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    return (Three a b c)

type ThreeFC = Three Int Int Int -> IntToInt -> IntToInt -> Bool


-- 5.
data Three' a b = Three' a b b deriving (Eq, Show)

instance Functor (Three' a) where
  fmap f (Three' a b b') = (Three' a) (f b) (f b')

instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    b' <- arbitrary
    return (Three' a b b')

type Three'FC = Three' Int Int -> IntToInt -> IntToInt -> Bool


-- 6.
data Four a b c d = Four a b c d deriving (Eq, Show)

instance Functor (Four a b c) where
  fmap f (Four a b c d) = (Four a b c) (f d)

instance (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d)
         => Arbitrary (Four a b c d) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    d <- arbitrary
    return (Four a b c d)

type FourFC = Four Int Int Int Int -> IntToInt -> IntToInt -> Bool


-- 7.
data Four' a b = Four' a a a b deriving (Eq, Show)

instance Functor (Four' a) where
  fmap f (Four' a a' a'' b) = (Four' a a' a'') (f b)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Four' a b) where
  arbitrary = do
    a <- arbitrary
    a' <- arbitrary
    a'' <- arbitrary
    b <- arbitrary
    return (Four' a a' a'' b)

type Four'FC = Four' Int Int -> IntToInt -> IntToInt -> Bool

-- 8.
-- No, because the kind of Trivial is *, and a Functor must have a
-- kind of * -> * at least. There is nothing to lift out.


main :: IO ()
main = do
  putStrLn "Identity a"
  quickCheck (functorIdentity :: Identity Int -> Bool)
  quickCheck (functorCompose' :: IdentityFC)
  putStrLn "Pair a a"
  quickCheck (functorIdentity :: Pair Int -> Bool)
  quickCheck (functorCompose' :: PairFC)
  putStrLn "Two a b"
  quickCheck (functorIdentity :: Two Int Int -> Bool)
  quickCheck (functorCompose' :: TwoFC)
  putStrLn "Three a b c"
  quickCheck (functorIdentity :: Three Int Int Int -> Bool)
  quickCheck (functorCompose' :: ThreeFC)
  putStrLn "Three' a b"
  quickCheck (functorIdentity :: Three' Int Int -> Bool)
  quickCheck (functorCompose' :: Three'FC)
  putStrLn "Four a b c d"
  quickCheck (functorIdentity :: Four Int Int Int Int -> Bool)
  quickCheck (functorCompose' :: FourFC)
  putStrLn "Four' a b"
  quickCheck (functorIdentity :: Four' Int Int -> Bool)
  quickCheck (functorCompose' :: Four'FC)
