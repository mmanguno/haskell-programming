data Sum a b =
    First a
  | Second b
  deriving (Eq, Show)


-- 1.

instance Functor (Sum a) where
  fmap f (First a)  = First a
  fmap f (Second b) = Second $ f b


-- 2.
-- It's not possible because it is the "inner" type. You're only
-- able to apply the function to the outermost type. You would have to
-- create a different datatype whose type constructor is "Sum b a" to
-- do that.
