module MonoidExercises where

import Data.Monoid
import Test.Hspec
import Test.QuickCheck ( Arbitrary
                       , arbitrary
                       , CoArbitrary
                       , frequency
                       , quickCheck )
import MonoidTest

-- 1.
data Trivial = Trivial deriving (Eq, Show)

instance Semigroup Trivial where
  _ <> _ = Trivial

instance Monoid Trivial where
  mempty = Trivial

instance Arbitrary Trivial where
  arbitrary = return Trivial

type TrivAssoc = Trivial -> Trivial -> Trivial -> Bool

-- 2.
newtype Identity a =
  Identity a deriving (Eq, Show)

instance Semigroup a => Semigroup (Identity a) where
  (Identity a) <> (Identity a') = Identity $ a <> a'

instance Monoid a => Monoid (Identity a) where
  mempty = Identity mempty

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return (Identity a)

type IdAssoc =
  Identity (Sum Int) -> Identity (Sum Int) -> Identity (Sum Int) -> Bool

-- 3.
data Two a b =
  Two a b deriving (Eq, Show)

instance (Semigroup a, Semigroup b) => Semigroup (Two a b) where
  (Two a b) <> (Two a' b') = Two (a <> a') (b <> b')

instance (Monoid a, Monoid b) => Monoid (Two a b) where
  mempty = Two mempty mempty

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return $ Two a b

type TwoAssoc =
     Two (Sum Int) (Sum Int)
  -> Two (Sum Int) (Sum Int)
  -> Two (Sum Int) (Sum Int)
  -> Bool


-- 4.
newtype BoolConj =
  BoolConj Bool deriving (Eq, Show)

instance Semigroup BoolConj where
  (BoolConj b) <> (BoolConj b') = BoolConj $ b && b'

instance Monoid BoolConj where
  mempty = BoolConj True

instance Arbitrary BoolConj where
  arbitrary = frequency [ (1, return $ BoolConj True)
                        , (1, return $ BoolConj False) ]

type BoolCAssoc =
  BoolConj -> BoolConj -> BoolConj -> Bool

-- 5.
newtype BoolDisj =
  BoolDisj Bool deriving (Eq, Show)

instance Semigroup BoolDisj where
  (BoolDisj b) <> (BoolDisj b') = BoolDisj $ b || b'

instance Monoid BoolDisj where
  mempty = BoolDisj False

instance Arbitrary BoolDisj where
  arbitrary = frequency [ (1, return $ BoolDisj True)
                        , (1, return $ BoolDisj False) ]

type BoolDAssoc =
  BoolDisj -> BoolDisj -> BoolDisj -> Bool


-- 6.

newtype Combine a b =
  Combine { unCombine :: (a -> b) }

instance Semigroup b => Semigroup (Combine a b) where
  (Combine f) <> (Combine g) = Combine $ \x -> ((f x) <> (g x))

instance Monoid b => Monoid (Combine a b) where
  mempty = Combine $ const mempty

instance (CoArbitrary a, Arbitrary b) => Arbitrary (Combine a b) where
  arbitrary = do
    a <- arbitrary
    return (Combine a)

type CombineInt = Combine Int (Sum Int)

-- 7.

newtype Comp a =
  Comp { unComp :: (a -> a) }

instance Semigroup (Comp a) where
  (Comp f) <> (Comp g) = Comp $ (f . g)

instance Monoid (Comp a) where
  mempty = Comp $ id

instance (CoArbitrary a, Arbitrary a) => Arbitrary (Comp a) where
  arbitrary = do
    a <- arbitrary
    return (Comp a)

type CompInt = Comp (Sum Int)


-- 8.
newtype Mem s a =
  Mem {
    runMem :: s -> (a,s)
  }

instance Semigroup a => Semigroup (Mem s a) where
  (Mem f) <> (Mem g) = Mem go
    where go x = (a, s)
            where a = fst (f x) <> fst (g x)
                  s = snd . f . snd . g $ x

instance Monoid a => Monoid (Mem s a) where
  mempty = Mem $ \s -> (mempty, s)

testMem = do
  let f' = Mem $ \s -> ("hi", s + 1)
      rmzero = runMem mempty 0
      rmleft = runMem (f' <> mempty) 0
      rmright = runMem (mempty <> f') 0
  print $ rmleft
  print $ rmright
  print $ (rmzero :: (String, Int))
  print $ rmleft == runMem f' 0
  print $ rmright == runMem f' 0

main :: IO ()
main = do
  putStrLn "Trivial:"
  quickCheck (monoidAssoc :: TrivAssoc)
  quickCheck (monoidLeftIdentity :: Trivial -> Bool)
  quickCheck (monoidRightIdentity :: Trivial -> Bool)
  putStrLn "Identity:"
  quickCheck (monoidAssoc :: IdAssoc)
  quickCheck (monoidLeftIdentity :: Identity (Sum Int) -> Bool)
  quickCheck (monoidRightIdentity :: Identity (Sum Int) -> Bool)
  putStrLn "Two:"
  quickCheck (monoidAssoc :: TwoAssoc)
  quickCheck (monoidLeftIdentity :: Two (Sum Int) (Sum Int) -> Bool)
  quickCheck (monoidRightIdentity :: Two (Sum Int) (Sum Int) -> Bool)
  putStrLn "BoolConj:"
  quickCheck (monoidAssoc :: BoolCAssoc)
  quickCheck (monoidLeftIdentity :: BoolDisj -> Bool)
  quickCheck (monoidRightIdentity :: BoolDisj -> Bool)
  putStrLn "BoolDisj:"
  quickCheck (monoidAssoc :: BoolDAssoc)
  quickCheck (monoidLeftIdentity :: BoolDisj -> Bool)
  quickCheck (monoidRightIdentity :: BoolDisj -> Bool)

  -- need to hspec these; no instances of Show or Eq for functions, and
  -- I don't want to try and delve into some Haskell black magic to make
  -- that """"""""work"""""""".
  hspec $ do
    describe "Combine" $ do
      it "combines two lambdas of Semigroups (1)" $ do
        (unCombine (f <> g) $ 0) `shouldBe` (Sum 0)
      it "combines two lambdas of Semigroups (2)" $ do
        (unCombine (f <> g) $ 1) `shouldBe` (Sum 2)
      it "combines two lambdas of Semigroups (3)" $ do
        (unCombine (f <> f) $ 1) `shouldBe` (Sum 4)
      it "combines two lambdas of Semigroups (4)" $ do
        (unCombine (g <> f) $ 1) `shouldBe` (Sum 2)
      it "should be associative" $ do
        (unCombine (g <> f) $ 1) `shouldBe` (unCombine (f <> g) $ 1)
      it "should be right-identitive" $ do
        (unCombine (f <> mempty) $ 1) `shouldBe` (unCombine f $ 1)
      it "should be left-identitive" $ do
        (unCombine (mempty <> f) $ 1) `shouldBe` (unCombine f $ 1)

    describe "Comp" $ do
      it "composes two lambdas (1)" $ do
        (unComp (f' <> g') $ 0) `shouldBe` 0
      it "composes two lambdas (2)" $ do
        (unComp (f' <> g') $ 1) `shouldBe` 1
      it "composes two lambdas (3)" $ do
        (unComp (f' <> f') $ 1) `shouldBe` 3
      it "composes two lambdas (4)" $ do
        (unComp (g' <> f') $ 1) `shouldBe` 1
      it "should be associative" $ do
        (unComp (g' <> f') $ 1) `shouldBe` (unComp (f' <> g') $ 1)
      it "should be right-identitive" $ do
        (unComp (f' <> mempty) $ 1) `shouldBe` (unComp f' $ 1)
      it "should be left-identitive" $ do
        (unComp (mempty <> f') $ 1) `shouldBe` (unComp f' $ 1)

    describe "Mem" $ do
      it "is left-identitive" $ do
        rmleft `shouldBe` runMem f'' 0
      it "is right-identitive" $ do
        rmright `shouldBe` runMem f'' 0
      it "is associative" $ do
        runMem (f'' <> (g'' <> h'')) 2 `shouldBe` runMem ((f'' <> g'') <> h'') 2
      it "mappends the Monoid and chains the S-value" $ do
        runMem (f'' <> g'') 2 `shouldBe` ("hi there", 21)
      where f = Combine $ \n -> Sum (n + 1)
            g = Combine $ \n -> Sum (n - 1)
            f' = Comp $ \n -> n + 1
            g' = Comp $ \n -> n - 1
            f'' = Mem $ \s -> ("hi", s + 1)
            g'' = Mem $ \s -> (" there", s * 10)
            h'' = Mem $ \s -> (".", s ^ 2)
            rmleft = runMem (f'' <> mempty) 0
            rmright = runMem (mempty <> f'') 0
