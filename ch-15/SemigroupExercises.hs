module SemigroupExercises where

import Data.Monoid
import Test.Hspec
import Test.QuickCheck ( Arbitrary
                       , arbitrary
                       , CoArbitrary
                       , frequency
                       , quickCheck)

-- Semigroup exercises

semigroupAssoc :: (Eq m, Semigroup m)
               => m -> m -> m -> Bool
semigroupAssoc a b c =
  (a <> (b <> c)) == ((a <> b) <> c)

-- 1.
data Trivial = Trivial deriving (Eq, Show)

instance Semigroup Trivial where
  Trivial <> Trivial = Trivial

instance Arbitrary Trivial where
  arbitrary = return Trivial


-- 2
newtype Identity a = Identity a deriving (Eq, Show)

instance Semigroup a => Semigroup (Identity a) where
  (Identity a) <> (Identity a') = Identity (a <> a')


instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return (Identity a)


type IdAssoc =
  (Identity (Sum Int)) -> (Identity (Sum Int)) -> (Identity (Sum Int)) -> Bool

-- 3.
data Two a b = Two a b deriving (Eq, Show)

instance (Semigroup a, Semigroup b) => Semigroup (Two a b) where
  (Two a b) <> (Two a' b') = Two (a <> a') (b <> b')


instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    return (Two a b)

type TwoS = Two (Sum Int) (Sum Int)


-- 4.

data Three a b c = Three a b c deriving (Eq, Show)

instance (Semigroup a, Semigroup b, Semigroup c)
       => Semigroup (Three a b c) where
  (Three a b c) <> (Three a' b' c') =
    Three (a <> a') (b <> b') (c <> c')

instance (Arbitrary a, Arbitrary b, Arbitrary c)
       => Arbitrary (Three a b c) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    return (Three a b c)

type ThreeS = Three (Sum Int) (Sum Int) (Sum Int)


-- 5.

data Four a b c d = Four a b c d deriving (Eq, Show)

instance (Semigroup a, Semigroup b, Semigroup c, Semigroup d)
       => Semigroup (Four a b c d) where
  (Four a b c d) <> (Four a' b' c' d') =
    Four (a <> a') (b <> b') (c <> c') (d <> d')

instance (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d)
       => Arbitrary (Four a b c d) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    c <- arbitrary
    d <- arbitrary
    return (Four a b c d)

type FourS = Four (Sum Int) (Sum Int) (Sum Int) (Sum Int)


--6.

newtype BoolConj = BoolConj Bool deriving (Eq, Show)

instance Semigroup BoolConj where
  (BoolConj b) <> (BoolConj b') = BoolConj $ b && b'

instance Arbitrary BoolConj where
  arbitrary = do
    a <- arbitrary
    return (BoolConj a)


-- 7.

newtype BoolDisj = BoolDisj Bool deriving (Eq, Show)

instance Semigroup BoolDisj where
  (BoolDisj b) <> (BoolDisj b') = BoolDisj $ b || b'

instance Arbitrary BoolDisj where
  arbitrary = do
    a <- arbitrary
    return (BoolDisj a)


-- 8.

data Or a b =
    Fst a
  | Snd b
  deriving (Eq, Show)


instance Semigroup (Or a b) where
  (Snd a) <> _ = Snd a
  _       <> b = b

instance (Arbitrary a, Arbitrary b) => Arbitrary (Or a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    frequency [ (1, return (Fst a))
              , (1, return (Snd b))]

type OrInt = Or Int Int


-- 9.

newtype Combine a b =
  Combine { unCombine :: (a -> b) }

instance Semigroup b => Semigroup (Combine a b) where
  (Combine f) <> (Combine g) = Combine $ \x -> ((f x) <> (g x))

instance (CoArbitrary a, Arbitrary b) => Arbitrary (Combine a b) where
  arbitrary = do
    a <- arbitrary
    return (Combine a)

type CombineInt = Combine Int (Sum Int)

-- 10.

newtype Comp a =
  Comp { unComp :: (a -> a) }

instance Semigroup (Comp a) where
  (Comp f) <> (Comp g) = Comp $ (f . g)

instance (CoArbitrary a, Arbitrary a) => Arbitrary (Comp a) where
  arbitrary = do
    a <- arbitrary
    return (Comp a)

type CompInt = Comp (Sum Int)


-- 11.

data Validation a b =
  Failure a | Success b
  deriving (Eq, Show)

instance Semigroup a => Semigroup (Validation a b) where
  (Success a) <> _           = Success a
  _           <> (Success b) = Success b
  (Failure a) <> (Failure b) = Failure $ a <> b

instance (Arbitrary a, Arbitrary b) => Arbitrary (Validation a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    frequency [ (1, return $ Failure a)
              , (1, return $ Success b) ]

type ValidStrInt = Validation String Int

failure :: String -> ValidStrInt
failure = Failure

success :: Int -> ValidStrInt
success = Success

-- main = do
-- let failure :: String
-- -> Validation String Int
-- failure = Failure
-- success :: Int
-- -> Validation String Int
-- success = Success
-- print $ success 1 <> failure "blah"
-- print $ failure "woot" <> failure "blah"
-- print $ success 1 <> success 2
-- print $ failure "woot" <> success 2

-- Prelude> main
-- Success 1
-- Failure "wootblah"
-- Success 1
-- Success 2


main :: IO ()
main = do
  quickCheck (semigroupAssoc :: Trivial -> Trivial -> Trivial -> Bool)
  quickCheck (semigroupAssoc :: IdAssoc)
  quickCheck (semigroupAssoc :: TwoS -> TwoS -> TwoS -> Bool)
  quickCheck (semigroupAssoc :: ThreeS -> ThreeS -> ThreeS -> Bool)
  quickCheck (semigroupAssoc :: FourS -> FourS -> FourS -> Bool)
  quickCheck (semigroupAssoc :: BoolConj -> BoolConj -> BoolConj -> Bool)
  quickCheck (semigroupAssoc :: BoolDisj -> BoolDisj -> BoolDisj -> Bool)
  quickCheck (semigroupAssoc :: OrInt -> OrInt -> OrInt -> Bool)
  quickCheck (semigroupAssoc :: ValidStrInt -> ValidStrInt -> ValidStrInt -> Bool)

  -- need to hspec these; no instances of Show or Eq for functions, and
  -- I don't want to try and delve into some Haskell black magic to make
  -- that """"""""work"""""""".
  hspec $ do
    describe "Combine" $ do
      it "combines two lambdas of Semigroups (1)" $ do
        (unCombine (f <> g) $ 0) `shouldBe` (Sum 0)
      it "combines two lambdas of Semigroups (2)" $ do
        (unCombine (f <> g) $ 1) `shouldBe` (Sum 2)
      it "combines two lambdas of Semigroups (3)" $ do
        (unCombine (f <> f) $ 1) `shouldBe` (Sum 4)
      it "combines two lambdas of Semigroups (4)" $ do
        (unCombine (g <> f) $ 1) `shouldBe` (Sum 2)
      it "should be associative" $ do
        (unCombine (g <> f) $ 1) `shouldBe` (unCombine (f <> g) $ 1)

    describe "Comp" $ do
      it "composes two lambdas (1)" $ do
        (unComp (f' <> g') $ 0) `shouldBe` 0
      it "composes two lambdas (2)" $ do
        (unComp (f' <> g') $ 1) `shouldBe` 1
      it "composes two lambdas (3)" $ do
        (unComp (f' <> f') $ 1) `shouldBe` 3
      it "composes two lambdas (4)" $ do
        (unComp (g' <> f') $ 1) `shouldBe` 1
      it "should be associative" $ do
        (unComp (g' <> f') $ 1) `shouldBe` (unComp (f' <> g') $ 1)

    describe "Validation" $ do
      it "chooses Success over Failure (1)" $ do
        success 1 <> failure "blah" `shouldBe` Success 1
      it "chooses Success over Failure (2)" $ do
        failure "woot" <> success 2 `shouldBe` Success 2
      it "takes the first Success" $ do
        success 1 <> success 2 `shouldBe` Success 1
      it "combines two failures" $ do
        failure "woot" <> failure "blah" `shouldBe` Failure "wootblah"
      where f = Combine $ \n -> Sum (n + 1)
            g = Combine $ \n -> Sum (n - 1)
            f' = Comp $ \n -> n + 1
            g' = Comp $ \n -> n - 1
