module ChapterExercises where

import Data.Monoid
import Test.Hspec
import Test.QuickCheck

-- Semigroup exercises
-- see SemigroupExercises.hs


-- Monoid exercises
-- see MonoidExercises.hs
