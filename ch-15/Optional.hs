module Optional where

import Test.QuickCheck

data Optional a =
    Nada
  | Only a
  deriving (Eq, Show)

instance Arbitrary a => Arbitrary (Optional a) where
  arbitrary = do
    a <- arbitrary
    frequency [ (1, return Nada)
              , (1, return (Only a)) ]


-- new after the book's publishing:
-- https://www.reddit.com/r/haskellquestions/comments/93cug0/is_semigroup_a_superclass_of_monoid/
instance Semigroup a => Semigroup (Optional a) where
  Nada   <> a       = a
  a      <> Nada    = a
  Only a <> Only a' = Only $ a <> a'


instance Monoid a => Monoid (Optional a) where
  mempty = Nada
