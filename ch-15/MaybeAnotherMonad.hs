module MaybeAnotherMonad where

import Test.QuickCheck
import MonoidTest
import Optional

newtype First' a =
  First' { getFirst' :: Optional a }
  deriving (Eq, Show)

instance Arbitrary a => Arbitrary (First' a) where
  arbitrary = do
    a <- arbitrary
    return (First' a)

instance Semigroup (First' a) where
  First' Nada <> a = a
  a           <> _ = a

instance Monoid (First' a) where
  mempty = First' Nada

-- this isn't used anywhere
--firstMappend :: First' a
--             -> First' a
--             -> First' a
--firstMappend = mappend


type FirstMappend =
     First' String
  -> First' String
  -> First' String
  -> Bool

type FstId =
  First' String -> Bool

main :: IO ()
main = do
  quickCheck (monoidAssoc :: FirstMappend)
  quickCheck (monoidLeftIdentity :: FstId)
  quickCheck (monoidRightIdentity :: FstId)
