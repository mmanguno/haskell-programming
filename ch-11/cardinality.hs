module Cardinality where

-- 1.
data PugType = PugData

-- cardinality of 1

-- 2.
data Airline =
       PapuAir
     | CatapultsR'Us
     | TakeYourChancesUnited

-- cardinality of 3

-- 3.

-- 2 ^ 16 = 65536

-- 4.
-- Int has a cardinality of 18446744073709551616.
-- Integer has an infinite cardinality (no minBound or maxBound)

-- 5.
-- Int8 has 8 bits => (2^8) possible numbers => 256 numbers => 256 elements.
