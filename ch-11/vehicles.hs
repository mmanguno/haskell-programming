module Vehicles where

data Price =
  Price Integer deriving (Eq, Show)

data Manufacturer =
    Mini
  | Mazda
  | Tata
  deriving (Eq, Show)

data Airline =
    PapuAir
  | CatapultsR'Us
  | TakeYourChancesUnited
  deriving (Eq, Show)

data Vehicle = Car Manufacturer Price
             | Plane Airline
             deriving (Eq, Show)

myCar    = Car Mini (Price 14000)
urCar    = Car Mazda (Price 20000)
clownCar = Car Tata (Price 7000)
doge     = Plane PapuAir


-- 1.
-- Vehicle

-- 2.

isCar :: Vehicle -> Bool
isCar (Car _ _) = True
isCar _         = False

isPlane :: Vehicle -> Bool
isPlane (Plane _) = True
isPlane _         = False

areCars :: [Vehicle] -> [Bool]
areCars = map isCar


-- 3.

getManu :: Vehicle -> Maybe Manufacturer
getManu (Plane _) = Nothing
getManu (Car m _) = Just m

-- 4.
-- it would return bottom, due to it being a partial function.
-- return a Maybe Manufacturer to correct this.

-- 5.

data Size = Size Integer deriving (Eq, Show)

data Vehicle' = Car' Manufacturer Price
             | Plane' Airline Size
             deriving (Eq, Show)


isPlane' :: Vehicle' -> Bool
isPlane' (Plane' _ _) = True
isPlane' _         = False

getManu' :: Vehicle' -> Maybe Manufacturer
getManu' (Plane' _ _) = Nothing
getManu' (Car' m _)   = Just m
