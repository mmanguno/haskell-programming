module DogTypes where

data Doggies a =
    Husky a
  | Mastiff a
  deriving (Eq, Show)

-- 1.
-- Doggies is a type constructor.

-- 2.
-- * -> *

-- 3.
-- *

-- 4.
-- Num a => Doggies a

-- 5.
-- Doggies Integer

-- 6.
-- Doggies String

data DogueDeBordeaux doge = DogueDeBordeaux doge

-- 7.
-- Both, though the compiler knows which to use

-- 8.
-- doge -> DogueDeBordeaux doge

-- 9.
-- DogueDebordeaux String
