module ChapterExercises where

-- Multiple choice

-- 1.
-- a) Weekday is a type with five data constructors

-- 2.
-- c) f :: Weekday -> String

-- 3.
-- b) must begin with a capital letter

-- 4.
-- c) delivers the final element of xs


-- ciphers

import Data.Char

shift :: Int -> Char -> Char
shift i c = letter
  where
    start   = ord 'a'
    shifted = i + (ord c)
    index   = mod (shifted - start) 26
    letter  = chr (index + start)


unshift :: Int -> Char -> Char
unshift = shift . negate


-- Vigenere which can shift or unshift, using some operation 'op'
vigenere' :: (Int -> Char -> Char) -> String -> String -> String
vigenere' op keyword []       = []
vigenere' op keyword (' ':xs) =     ' ' : (vigenere' op keyword xs)
vigenere' op keyword (x:xs)   = shifted : (vigenere' op newKey xs)
  where
    shifter = head keyword
    shiftBy = ord shifter - ord 'a'
    shifted = op shiftBy x
    newKey  = tail keyword ++ [shifter]


vigenere :: String -> String -> String
vigenere = vigenere' shift


unVigenere :: String -> String -> String
unVigenere = vigenere' unshift


testString = "meet at dawn"
cipher     = "ally"
ciphered   = vigenere cipher testString
unCiphered = unVigenere cipher ciphered
cipherTest =
  if ciphered == "mppr ae oywy"
  then "Vigenere Success"
  else "Failed to cipher: '" ++ ciphered ++ "'"

unCipherTest =
  if unCiphered == testString
  then "Uncipher Success"
  else "Failed to uncipher: '" ++ unCiphered ++ "'"


-- As-patterns

-- 1.
-- logic: continually popping off elements from the subsequence
-- list; if theres no more elements, then that means the whole
-- subsequence appears in order in the lager list
isSubseqOf :: (Eq a) => [a] -> [a] -> Bool
isSubseqOf [] _ = True
isSubseqOf _ [] = False
isSubseqOf sub@(a:as) (x:xs) =
  if a == x
  then isSubseqOf as xs
  else isSubseqOf sub xs


-- 2.
capitalizeWords :: String -> [(String, String)]
capitalizeWords s = foldr f [] (words s)
  where
    f [] acc          = acc
    f word@(x:xs) acc = (word, (toUpper x):xs) : acc


-- Language exercises

-- 1.
capitalizeWord :: String -> String
capitalizeWord []     = []
capitalizeWord (x:xs) = toUpper x : xs


-- 2.
capitalizeParagraph :: String -> String
capitalizeParagraph s = go (capitalizeWord s)
  where
    go []            = []
    go ('.':' ':xs)  = '.':' ':(capitalizeWord (go xs))
    go (x:xs)        = x:go xs


-- Phone exercise
-- (see phone.hs)


-- Hutton's Razor

-- (see hutton.hs)
