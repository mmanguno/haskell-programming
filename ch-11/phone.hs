module Phone where

import Data.Char

-- 1.

data DaPhone = DaPhone [Key]
  deriving (Eq, Show)

data Key =
  Key { digit :: Char
      , letters :: [Char] }
  deriving (Eq, Show)

-- enforces the constraint that all key's primary
-- 'digits' appear as the last character in their
-- accosiated Char list. e.g. '4' -> "ghi4"
makeKey :: Char -> [Char] -> Key
makeKey c ls = Key c (ls ++ [c])

-- These should be wrapped in Maybes, but for brevity's sake
-- (and since we haven't covered it fully), just going
-- without them.

-- Given a phone and Char, return the Key that contains the Char.
correspondingKey :: DaPhone -> Char -> Key
correspondingKey (DaPhone (k:ks)) c
  | c `elem` (letters k) = k
  | otherwise            = correspondingKey (DaPhone ks) c

-- Given a key and char, return the number
-- of 'presses' required to get to the Char.
keyPresses :: Key -> Char -> Int
keyPresses k c = go (letters k) c 1
  where
    go (l:ls) c count
      | c == l    = count
      | otherwise = go ls c (count + 1)

phone = DaPhone
  [
   makeKey '1' "",
   makeKey '2' "abc",
   makeKey '3' "def",
   makeKey '4' "ghi",
   makeKey '5' "jkl",
   makeKey '6' "mno",
   makeKey '7' "pqrs",
   makeKey '8' "tuv",
   makeKey '9' "wxyz",
   makeKey '*' "^",
   makeKey '0' "+ _",
   makeKey '#' ".,"
  ]

-- 2.

convo :: [String]
convo =
  ["Wanna play 20 questions",
    "Ya",
    "U 1st haha",
    "Lol ok. Have u ever tasted alcohol",
    "Lol ya",
    "Wow ur cool haha. Ur turn",
    "Ok. Do u think I am pretty Lol",
    "Lol ya",
    "Just making sure rofl ur turn"]

-- validButtons = "1234567890*#"
type Digit = Char

-- Valid presses: 1 and up
type Presses = Int


reverseTaps :: DaPhone
            -> Char
            -> [(Digit, Presses)]
reverseTaps phone@(DaPhone keys) c
  | isUpper c = (reverseTaps phone '^') ++ (reverseTaps phone (toLower c))
  | otherwise = [(digit', presses)]
  where
    key = correspondingKey phone c
    presses = keyPresses key c
    digit' = digit key

testTaps =
  if reverseTaps phone 'A' == [('*', 1), ('2', 1)]
  then "Success!"
  else "Failure"


cellPhonesDead :: DaPhone
               -> String
               -> [(Digit, Presses)]
cellPhonesDead phone s = foldr ((++) . reverseTaps phone) [] s

-- 3.

fingerTaps :: [(Digit, Presses)] -> Presses
fingerTaps = foldr ((+) . snd) 0

-- 4.
-- what was the most popular letter?
-- (going with O(n^2) algorithm since i would normally use maps)
itemToCount :: Eq a =>  [a] -> [(a, Int)]
itemToCount []  = []
itemToCount as = go as
  where
    go [] = []
    go (x:xs)
      | x `elem` xs = go xs  -- skip duplicates
      | otherwise   = (x, count x as) : (go xs)
    count i l = foldr (\v acc -> if i == v then acc + 1 else acc) 0 l

-- finds the most numerous item (O(n))
maxItem :: Ord a => [(a, Int)] -> (a, Int)
maxItem s = go (tail s) ((fst . head) s) ((snd . head) s)
  where
    go :: Ord b => [(b, Int)] -> b -> Int -> (b, Int)
    go [] curItem curMax = (curItem, curMax)
    go ((i, c):xs) curItem curMax
      | c > curMax = go xs i c
      | otherwise  = go xs curItem curMax

mostPopularLetter :: String -> Char
mostPopularLetter s = fst . maxItem . letterCounts $ s
  where
    -- assuming we don't want spaces
    letterCounts xs = filter ((/= ' ') . fst) (itemToCount xs)

-- what was it's cost
mostPopularLetterCost :: DaPhone -> String -> Int
mostPopularLetterCost phone s = count * cost
  where
    withoutSpaces   = filter (/=' ') s
    (letter, count) = maxItem . itemToCount $ withoutSpaces
    cost            = (fingerTaps . reverseTaps phone) letter


-- 5.
coolestLtr :: [String] -> Char
coolestLtr = mostPopularLetter . concat

concatWith :: ([a] -> [b] -> [b]) -> [[a]] -> [b]
concatWith _ []     = []
concatWith f (x:xs) = x `f` (concatWith f xs)

coolestWord :: [String] -> String
coolestWord s = fst . maxItem . itemToCount $ words'
  where
    -- do some transformations on the paragraph
    alphanumeric     = ['a'..'z'] ++ ['0'..'9']
    concatWithSpaces = concatWith (\a b -> a ++ (' ':b))
    combined         = concatWithSpaces s
    lowered          = map toLower combined
    alphanumericOnly = filter (`elem` ' ':alphanumeric) lowered
    words'           = words alphanumericOnly
