module Quad where

-- 1.
data Quad = One
          | Two
          | Three
          | Four
          deriving (Eq, Show)


-- how many different forms can this take?
eQuad :: Either Quad Quad
eQuad = undefined

-- 16 (4 * 4)

-- 2.
prodQuad :: (Quad, Quad)

-- 16 (4 * 4)

-- 3.
funcQuad :: Quad -> Quad

-- 256 (4 ^ 4)

-- 4.
prodTBool :: (Bool, Bool, Bool)

-- 8 (2 * 2 * 2)

-- 5.
gTwo :: Bool -> Bool -> Bool

-- 16 ((2^2)^2)


-- 6.
fTwo :: Bool -> Quad -> Quad

-- 65536 ((4 ^ 4) ^ 2)
