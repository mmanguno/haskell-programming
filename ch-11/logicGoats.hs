{-# LANGUAGE FlexibleInstances #-}

class TooMany a where
  tooMany :: a -> Bool

-- 1.
instance TooMany (Int, String) where
  tooMany (i, _) = i > 42

-- 2.
instance TooMany (Int, Int) where
  tooMany (i, j) = (i + j) > 42

-- 3.
instance (Num a, TooMany a) => TooMany (a, a) where
  tooMany (a, b) = tooMany (a + b)
