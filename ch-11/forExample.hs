module ForExample where

-- 1.

data Example = MakeExample deriving Show

-- :t MakeExample
-- MakeExample :: Example
-- :t Example
-- <Error: no Data Constructor for Example>

-- 2.
-- :info Example
-- data Example = MakeExample      -- Defined at forExample.hs:5:1
-- instance [safe] Show Example -- Defined at forExample.hs:5:37

-- It has an instance of Show (from the deriving clause)


-- 3.

data Example' = MakeExample' Int deriving Show

-- :t MakeExample'
-- MakeExample' :: Int -> Example'
