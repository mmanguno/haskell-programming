-- 1.
-- 4. 2 possible values in Big Bool, 2 possible values in Small Bool.


-- bring Int8 in scope
import Data.Int

data NumberOrBool =
    Numba Int8
  | BoolyBool Bool
  deriving (Eq, Show)

-- parentheses due to syntactic
-- collision between (-) minus
-- and the negate function
myNumba = Numba (-128)

-- 2.
-- 258. 256 from Numba Int8 and 2 from BoolyBool.
-- Literal is out of Int8 range.
